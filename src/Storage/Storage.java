package Storage;

import java.io.Serializable;
import java.util.ArrayList;

import model.Bestilling;
import model.Fadølsanlæg;
import model.Kunde;
import model.PrisListe;
import model.Produkt;
import model.Transaktion;

public class Storage implements Serializable {

	private static Storage storage;

	private static ArrayList<Kunde> kunder = new ArrayList<>();
	private static ArrayList<PrisListe> prislister = new ArrayList<>();
	private static ArrayList<Transaktion> transaktioner = new ArrayList<>();
	private static ArrayList<Produkt> produkter = new ArrayList<>();
	private static ArrayList<Bestilling> bestillinger = new ArrayList<>();
	private static ArrayList<Fadølsanlæg> fadølanlæg = new ArrayList<>();

	public static Storage getStorage() {
		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}

	// -------------------------------Kunde-----------------------------------------

	public ArrayList<Kunde> getKunder() {
		return new ArrayList<Kunde>(kunder);
	}

	public void addKunde(Kunde kunde) {
		kunder.add(kunde);
	}

	public void removeKunde(Kunde kunde) {
		kunder.remove(kunde);
	}

	// -------------------------------PrisListe--------------------------------------

	public ArrayList<PrisListe> getPrisListe() {
		return new ArrayList<PrisListe>(prislister);
	}

	public void addPrisListe(PrisListe prisliste) {
		prislister.add(prisliste);
	}

	public void removePrisListe(PrisListe prisliste) {
		prislister.remove(prisliste);
	}

	// -------------------------------Transaktion------------------------------------

	public ArrayList<Transaktion> getTransaktion() {
		return new ArrayList<Transaktion>(transaktioner);
	}

	public void addTransaktion(Transaktion transaktion) {
		transaktioner.add(transaktion);
	}

	public void removeTransaktion(Transaktion transaktion) {
		transaktioner.remove(transaktion);
	}

	// -------------------------------Produkt----------------------------------------

	public ArrayList<Produkt> getProdukt() {
		return new ArrayList<Produkt>(produkter);
	}

	public void addProdukt(Produkt produkt) {
		produkter.add(produkt);
	}

	public void removeProdukt(Produkt produkt) {
		produkter.remove(produkt);
	}
	// -------------------------------Bestilling-------------------------------------

	public ArrayList<Bestilling> getBestilling() {
		return new ArrayList<Bestilling>(bestillinger);
	}

	public void addBestilling(Bestilling bestilling) {
		bestillinger.add(bestilling);
	}

	public void removeBestilling(Bestilling bestilling) {
		bestillinger.remove(bestilling);
	}

	// -------------------------------Fadølsanlæg-------------------------------------
	
	public ArrayList<Fadølsanlæg> getFadølsanlæg() {
		return new ArrayList<Fadølsanlæg>(fadølanlæg);
	}

	public void addFadølsanlæg(Fadølsanlæg fadølsanlæg) {
		fadølanlæg.add(fadølsanlæg);
	}

	public void removeFadølsanlæg(Fadølsanlæg fadølsanlæg) {
		fadølanlæg.remove(fadølsanlæg);
	}
}
