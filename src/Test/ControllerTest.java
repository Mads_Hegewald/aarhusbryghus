package Test;

import static org.junit.Assert.*;

import java.time.LocalDate;


import org.junit.Test;

import Storage.Storage;
import controller.Controller;
import model.Bestilling;
import model.Betaling;
import model.Betalingstype;
import model.Kunde;
import model.PrisListe;
import model.Produkt;
import model.Transaktion;

public class ControllerTest {

	Controller controller = Controller.getController();
	private Storage storage = Storage.getStorage();
	
	
//	----------------------------------------------Kunde--------------------------------------------------
	@Test
	public void testCreateKunde() {

		//Arrange
		Kunde k1 = controller.createKunde("Hans", "Randersvej 1", 10);
		Kunde k2 = controller.createKunde("Bo", "Flisen 2", 3215123);
//		Kunde k3 = controller.createKunde("Karl", "Franzvej 5", 4532,24);
		
		//Act
		
		
		//Assert
		assertEquals("Hans", k1.getNavn());
		assertEquals("Bo", k2.getNavn());
//		assertEquals("Karl", k3.getNavn());
	}
	
//	----------------------------------------------PrisListe--------------------------------------------------
	
	@Test
	public void testCreatePrisliste() {

		//Arrange
		PrisListe p1 = controller.createPrisListe("Fredagsbar");
		PrisListe p2 = controller.createPrisListe("Butik");
		PrisListe p3 = controller.createPrisListe("Event");
		
		//Act
		
		
		//Assert
		
		assertEquals("Fredagsbar", p1.getBeskrivelse());
		assertEquals("Butik", p2.getBeskrivelse());
		assertEquals("Event", p3.getBeskrivelse());

	}
	
//	----------------------------------------------Transaktion--------------------------------------------------
	
	@Test
	public void testCreateTransaktion() {

		//Arrange
		Transaktion t1 = controller.createTransaktion(LocalDate.of(2019, 05, 10), 3);
		Transaktion t2 = controller.createTransaktion(LocalDate.now(), 4);
		Transaktion t3 = controller.createTransaktion(LocalDate.of(2019, 07, 15), 5);
		Transaktion t4 = controller.createTransaktion(LocalDate.of(2019, 07, 03), 6);
		
		
		//Act
		
		
		//Assert
		assertEquals(LocalDate.of(2019, 05, 10), t1.getDato());
		assertEquals(LocalDate.now(), t2.getDato());
		assertEquals(LocalDate.of(2019, 07, 15), t3.getDato());
		assertEquals(LocalDate.of(2019, 07, 03), t4.getDato());
		
	}
	
//	----------------------------------------------Produkt--------------------------------------------------
	
	@Test
	public void testCreateProdukt() {

		//Arrange
		Produkt øl = controller.createProdukt("Klosterbryg");
		Produkt fustage = controller.createProdukt("Fustage", 200.0, 25);
		Produkt klippekort = controller.createProdukt("Klippekort", 4);

		
		//Act
		
		
		//Assert
		
		assertEquals("Klosterbryg", øl.getBeskrivelse());
		assertEquals("Fustage", fustage.getBeskrivelse());
		assertEquals("Klippekort", klippekort.getBeskrivelse());

	}
	
//	----------------------------------------------Betaling--------------------------------------------------

	@Test
	public void testCreateBetaling() {

		//Arrange
		Betaling b1 = controller.createBetaling(400.0, Betalingstype.DANKORT);
		Betaling b2 = controller.createBetaling(345.72, Betalingstype.KONTANT);
//		Betaling b3 = controller.createBetaling(200.0, Betalingstype.SMÅSTEN);


		
		//Act
		
		
		//Assert
		
		assertEquals(400.0, b1.getBeløb(), 0.01);
		assertEquals(345.72, b2.getBeløb(), 0.01);
//		assertEquals(200, b3.getBeløb(), 0.01);

	}
	
//	----------------------------------------------Bestilling--------------------------------------------------
	
	@Test
	public void testCreateFadølsanlægBestilling() {

		//Arrange
		controller.createFadølsanlæg("Fadølsanlæg 2", false);
		controller.createFadølsanlæg("Fadølsanlæg 6", true);

		
		controller.createFadølsanlægBestilling("5", LocalDate.of(2019, 10, 10), LocalDate.of(2019, 10, 15)); 
		controller.createFadølsanlægBestilling("6", LocalDate.of(2019, 10, 13), LocalDate.of(2019, 10, 20)); 
		controller.createFadølsanlægBestilling("7", LocalDate.of(2019, 10, 21), LocalDate.of(2019, 10, 24)); 
		Bestilling b = controller.createFadølsanlægBestilling("8", LocalDate.of(2019, 10, 22), LocalDate.of(2019, 10, 27)); 
		controller.createFadølsanlægBestilling("9", LocalDate.of(2019, 10, 27), LocalDate.of(2019, 10, 27)); 
		

		//Act
		
		
		String s = storage.getBestilling().get(0).getBasicBeskrivelse();
		String s1 = storage.getBestilling().get(1).getBasicBeskrivelse();
		String s2 = storage.getBestilling().get(2).getBasicBeskrivelse();
		String s4 = storage.getBestilling().get(3).getBasicBeskrivelse();
		
		//Assert
		
		
		assertEquals("Fadølsanlæg 2", s);
		assertEquals("Fadølsanlæg 6", s1);
		assertEquals("Fadølsanlæg 2", s2);
		assertEquals(null, b);
		assertEquals("Fadølsanlæg 6", s4);
	}


}
