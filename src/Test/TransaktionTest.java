package Test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import model.Fustage;
import model.Klippekort;
import model.Transaktion;
import model.TransaktionsListeLinje;
import model.Øl;

public class TransaktionTest {

	@Test
	public void testGetSamletBeløb() {
		
		//Arrange
		Fustage f = new Fustage("Fustage", 200.0, 30);
		Transaktion t = new Transaktion(LocalDate.of(2019, 10, 10), 5);
		TransaktionsListeLinje tll = new TransaktionsListeLinje(200.0, 4);
		t.addTll(tll);
		f.setTll(tll);
		
		Øl øl = new Øl("Kloster");
		Transaktion t1 = new Transaktion(LocalDate.of(2019, 10, 15), 10);
		TransaktionsListeLinje tll1 = new TransaktionsListeLinje(50.0, 2);
		t1.addTll(tll1);
		øl.setTll(tll1);
		
		Øl øl1 = new Øl("Ale");
		Transaktion t2 = new Transaktion(LocalDate.of(2019, 10, 30), 2);
		TransaktionsListeLinje tll2 = new TransaktionsListeLinje(72.0, 7);
		t2.addTll(tll2);
		øl1.setTll(tll2);
		
		Klippekort k = new Klippekort("Klippekort", 4);
		Transaktion t3 = new Transaktion(LocalDate.of(2019, 04, 02), 4);
		TransaktionsListeLinje tll3 = new TransaktionsListeLinje(100.0, 2);
		t3.addTll(tll3);
		k.setTll(tll3);
		
		
		//Act
		Double beløb = t.getSamletBeløb();
		Double beløb1 = t1.getSamletBeløb();
		Double beløb2 = t2.getSamletBeløb();
		Double beløb3 = t3.getSamletBeløb();
		
		int fNr = t.getFakturaNr();
		int fNr1 = t1.getFakturaNr();
		
		//Assert
		assertEquals(800.0, beløb, 0.1);
		assertEquals(100.0, beløb1, 0.1);
		assertEquals(504.0, beløb2, 0.1);
		assertEquals(200.0, beløb3, 0.1);
		assertEquals(5, fNr, 0.1);
		assertEquals(10, fNr1, 0.1);
		
	}

}
