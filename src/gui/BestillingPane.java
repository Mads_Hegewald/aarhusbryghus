package gui;

import java.time.format.DateTimeFormatter;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import model.Bestilling;
import model.Fadølsanlæg;
import model.Kunde;

public class BestillingPane extends GridPane {
	private TextArea txaKundeinfo, txaBestillingsinfo, txaFadølsanlæg;
	private ListView<Bestilling> lvwBestillinger;

	public BestillingPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		Label lblBestillinger = new Label("Bestillinger");
		this.add(lblBestillinger, 0, 0);

		lvwBestillinger = new ListView<>();
		this.add(lvwBestillinger, 0, 1, 1, 5);
		lvwBestillinger.setPrefWidth(200);
		lvwBestillinger.setPrefHeight(200);

		lvwBestillinger.getItems().setAll(Controller.getController().getBestillinger());

		ChangeListener<Bestilling> listener = (ov, oldBestilling, newBestilling) -> this.selectedBestillingChanged();
		lvwBestillinger.getSelectionModel().selectedItemProperty().addListener(listener);

		lvwBestillinger.getSelectionModel().clearSelection();

		Label lblKundeinfo = new Label("Kundeinfo:");
		this.add(lblKundeinfo, 2, 0);

		txaKundeinfo = new TextArea();
		this.add(txaKundeinfo, 2, 1);
		txaKundeinfo.setPrefSize(200, 125);
		txaKundeinfo.setEditable(false);

		Label lblBestillingsinfo = new Label("Bestillingsinfo:");
		this.add(lblBestillingsinfo, 2, 3);

		txaBestillingsinfo = new TextArea();
		this.add(txaBestillingsinfo, 2, 4);
		txaBestillingsinfo.setPrefSize(200, 125);
		txaBestillingsinfo.setEditable(false);

		txaFadølsanlæg = new TextArea();
		this.add(txaFadølsanlæg, 3, 1);
		txaFadølsanlæg.setPrefSize(200, 200);
		txaFadølsanlæg.setEditable(false);

		Button btnOpdater = new Button();
		btnOpdater.setText("Opdater Bestilling");
		this.add(btnOpdater, 2, 5);
		btnOpdater.setOnAction(event -> this.updateAction());

		Button btnSlet = new Button();
		btnSlet.setText("Slet Bestilling");
		this.add(btnSlet, 2, 6);
		btnSlet.setOnAction(event -> this.deleteAction());

		Button btnOpret = new Button();
		btnOpret.setText("Opret Bestilling");
		this.add(btnOpret, 2, 7);
		btnOpret.setOnAction(event -> this.createAction());

		this.updateControls();

	}

	// -------------------------------------------------------------------------

	private void selectedBestillingChanged() {
		this.updateControls();
	}

	public void updateControls() {
		// Info til txaKundeinfo
		lvwBestillinger.getItems().setAll(Controller.getController().getBestillinger());
		Bestilling bestilling = lvwBestillinger.getSelectionModel().getSelectedItem();
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < Controller.getController().getKunder().size(); i++) {
			for (int k = 0; k < Controller.getController().getKunder().get(i).getTransaktioner().size(); k++) {
				for (int j = 0; j < Controller.getController().getKunder().get(i).getTransaktioner().get(k)
						.getTransaktionsListeLinje().size(); j++) {
					if (Controller.getController().getKunder().get(i).getTransaktioner().get(k)
							.getTransaktionsListeLinje().get(j).getProdukt() == bestilling) {
						Kunde kunde;
						kunde = Controller.getController().getKunder().get(i);
						sb.append("Navn: " + kunde.getNavn() + "\n" + "Tlf: " + kunde.getTlfnr() + "\n" + "Adresse: "
								+ kunde.getAdresse());
					}

				}
			}
			txaKundeinfo.setText(sb.toString());

		}

		// Info for txaBestillingsInfo
		StringBuilder sb1 = new StringBuilder();
		if (bestilling != null) {
			String s = (bestilling.getBasicBeskrivelse() + "\n" + "id: " + bestilling.getId() + "\n" + "Start Dato: "
					+ bestilling.getStartDato() + "\n" + "Slut Dato: " + bestilling.getSlutDato());
			if (bestilling.getBasicBeskrivelse().contains("Fadølsanlæg")) {
				sb1.append(s + "\n" + "Pris: " + bestilling.getTll().getPris() + " kr.");
			} else {
				sb1.append(s + "\n" + "Antal Personer: " + bestilling.getAntalPersoner() + "\n" + "Pris: "
						+ bestilling.getTll().getPris() + "\n" + "Tidspunkt: "
						+ bestilling.getTidspunkt().format(DateTimeFormatter.ofPattern("HH:mm")));
			}
			txaBestillingsinfo.setText(sb1.toString());
		}

		// Info om ledige fadølsanlæg
		StringBuilder sb2 = new StringBuilder();
		for (Fadølsanlæg f : Controller.getController().getFadølsanlæg())
			if (f.isAvailable() == true) {
				sb2.append(f.getBeskrivelse() + ": Fri " + "\n");
			} else {
				sb2.append(f.getBeskrivelse() + ": Ufri " + "\n");
			}

		txaFadølsanlæg.setText(sb2.toString());

	}

	private void createAction() {
		BestillingWindow dia = new BestillingWindow("Opret Bestilling");
		dia.showAndWait();

		// Wait for the modal dialog to close

		lvwBestillinger.getItems().setAll(Controller.getController().getBestillinger());
		int index = lvwBestillinger.getItems().size() - 1;
		lvwBestillinger.getSelectionModel().select(index);
	}

	private void updateAction() {
		Bestilling bestilling = lvwBestillinger.getSelectionModel().getSelectedItem();
		if (bestilling == null) {
			return;
		}

		BestillingWindow hw = new BestillingWindow("Opdatere Bestilling", bestilling);
		hw.showAndWait();

		// Wait for the modal dialog to close

		int selectIndex = lvwBestillinger.getSelectionModel().getSelectedIndex();
		lvwBestillinger.getItems().setAll(Controller.getController().getBestillinger());
		lvwBestillinger.getSelectionModel().select(selectIndex);
	}

	private void deleteAction() {
		Bestilling bestilling = lvwBestillinger.getSelectionModel().getSelectedItem();
		if (bestilling != null) {
			Controller.getController().removeBestilling(bestilling);
			lvwBestillinger.getItems().setAll(Controller.getController().getBestillinger());
			this.updateControls();
			txaBestillingsinfo.clear();
		}
	}

}
