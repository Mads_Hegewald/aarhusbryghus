package gui;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;


import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Produkt;
import model.TransaktionsListeLinje;

public class TransaktionPane extends GridPane {
	private TextField txfName, txfStartDate, txfEndDate, txfPrice;
	private TextArea txaSalgsOversigt, txaMånedsOversigt;
	private Label lblError, lblDato, lblSalg;
	private DatePicker dpDagsDato, dpSlut;
	Controller controller = Controller.getController();

	public TransaktionPane() {

		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		txaSalgsOversigt = new TextArea();
		txaSalgsOversigt.setPrefSize(200, 250);
		this.add(txaSalgsOversigt, 1, 2);

//		txaMånedsOversigt = new TextArea();
//		txaMånedsOversigt.setPrefSize(200, 250);
//		this.add(txaMånedsOversigt, 3, 2);

		lblDato = new Label("Dato: ");
		this.add(lblDato, 1, 0);
		GridPane.setHalignment(lblDato, HPos.LEFT);

	
		

		dpDagsDato = new DatePicker(LocalDate.now());
		this.add(dpDagsDato, 1, 0);
		GridPane.setHalignment(dpDagsDato, HPos.RIGHT);
		dpDagsDato.setPrefWidth(150);
		dpDagsDato.setEditable(false);
		dpDagsDato.setDayCellFactory(picker -> new DateCell() {
			public void updateItem(LocalDate date, boolean empty) {
				super.updateItem(date, empty);
				LocalDate today = LocalDate.now();

				setDisable(empty || date.compareTo(today) > 0);
			}
		});
		
		ChangeListener<? super LocalDate> dpSalgsListener = (ov, oldValue, newValue) -> startDatoSelected();
		dpDagsDato.valueProperty().addListener(dpSalgsListener);
		
		lblSalg = new Label("Total salg: " + getSamletBeløb());
		this.add(lblSalg, 1, 4);
		GridPane.setHalignment(lblDato, HPos.LEFT);
		
		initSalg();
	}
	
	private void startDatoSelected() {
		lblSalg.setText("Total salg: " + getSamletBeløb());
		txaSalgsOversigt.clear();
		initSalg();
	}
	

	public void initSalg() {
		txaSalgsOversigt.clear();
		HashMap<String, Integer> produkter = new HashMap<String, Integer>();
		for (int i = 0; i < controller.getTransaktion().size(); i++) {
			if(controller.getTransaktion().get(i).getDato().equals(dpDagsDato.getValue())) {
			for (int j = 0; j < controller.getTransaktion().get(i).getTransaktionsListeLinje().size(); j++) {
				if (controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j).getProdukt().getBeskrivelse()
						.contains("Rundvisning")
						|| controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j).getProdukt()
								.getBeskrivelse().contains("Fadølsanlæg")) {
				
				} else {
					if (!produkter.containsKey(controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j)
							.getProdukt().getBeskrivelse())) {
						produkter.put(
								controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j).getProdukt()
										.getBeskrivelse(),
								controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j).getAntal());
					} else {
						produkter.put(
								controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j).getProdukt()
										.getBeskrivelse(),
								produkter
										.get(controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j)
												.getProdukt().getBeskrivelse())
										+ controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j)
												.getAntal());
					}
				}
				}

			}

		}
		for (String s : produkter.keySet()) {
			txaSalgsOversigt.appendText(s + " " + produkter.get(s) + "\n");
		}

	}

	public Double getSamletBeløb() {
		Double samletBeløb = 0.0;
		for (int i = 0; i < controller.getTransaktion().size(); i++) {
			if(controller.getTransaktion().get(i).getDato().equals(dpDagsDato.getValue())) {
			for (int j = 0; j < controller.getTransaktion().get(i).getTransaktionsListeLinje().size(); j++) {
				for (TransaktionsListeLinje t : controller.getTransaktion().get(i).getTransaktionsListeLinje()) {
					if (controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j).getProdukt()
							.getBeskrivelse().contains("Rundvisning")
							|| controller.getTransaktion().get(i).getTransaktionsListeLinje().get(j).getProdukt()
									.getBeskrivelse().contains("Fadølsanlæg")) {

					} else {
						samletBeløb += (t.getPris() * t.getAntal());
					}
				}
				}
			}
		}
		return samletBeløb;
	}

}
