package gui;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Bestilling;

public class BarBestilling extends Stage {

	private static ListView<Bestilling> lvwBestillinger;
	Button btnBekræft = new Button("Bekræft");

	public BarBestilling() {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle("Bestillinger");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);

		Label lblBestillinger = new Label("Bestillinger");
		pane.add(lblBestillinger, 0, 0);

		lvwBestillinger = new ListView<>();
		pane.add(lvwBestillinger, 0, 4, 1, 5);
		lvwBestillinger.setPrefWidth(200);
		lvwBestillinger.setPrefHeight(200);

		ChangeListener<Bestilling> listenerBestilling = (ov, oldBestilling, newBestilling) -> this.selectedBestilling();
		lvwBestillinger.getSelectionModel().selectedItemProperty().addListener(listenerBestilling);

		initControls();
	}

	// -------------------------------------------------------------------------

	public static ListView<Bestilling> getLvwBestillinger() {
		return lvwBestillinger;
	}
	
	private void initControls() {
		lvwBestillinger.getItems().setAll(Controller.getController().getBestillinger());
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 9);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		pane.add(btnBekræft, 0, 9);
		GridPane.setHalignment(btnBekræft, HPos.RIGHT);
		btnBekræft.setOnAction(event -> this.bekræftAction());
		btnBekræft.setDisable(true);

	}

//	// -------------------------------------------------------------------------

	private Object selectedBestilling() {
		btnBekræft.setDisable(false);
		return null;
	}

	private Object cancelAction() {
		this.hide();
		return null;
	}

	private Object bekræftAction() {
		this.hide();
		return null;
	}

}
