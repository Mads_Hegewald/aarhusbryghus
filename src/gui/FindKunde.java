package gui;

import java.util.ArrayList;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Kunde;

public class FindKunde extends Stage {

	private static ListView<Kunde> lvwKunder;
	private Button btnBekræft = new Button("Bekræft");
	private Button btnUpdate = new Button("Opdater");


	public FindKunde() {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle("Kunder");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);

		Label lblPrislister = new Label("Kunder");
		pane.add(lblPrislister, 0, 0);

		lvwKunder = new ListView<>();
		pane.add(lvwKunder, 0, 4, 2, 5);
		lvwKunder.setPrefWidth(200);
		lvwKunder.setPrefHeight(200);

		ChangeListener<Kunde> listenerKunder = (ov, oldKunde, newKunde) -> this.selectedKunde();
		lvwKunder.getSelectionModel().selectedItemProperty().addListener(listenerKunder);

		initControls();
	}

	// -------------------------------------------------------------------------

	private void initControls() {
		ArrayList<Kunde> alKunder = new ArrayList<>(Controller.getController().getKunder());
		Kunde.selectionSort(alKunder);
		lvwKunder.getItems().setAll(alKunder);
	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 9);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		pane.add(btnBekræft, 1, 9);
		GridPane.setHalignment(btnBekræft, HPos.RIGHT);
		btnBekræft.setOnAction(event -> this.bekræftAction());
		btnBekræft.setDisable(true);

		pane.add(btnUpdate, 0, 10);
		GridPane.setHalignment(btnUpdate, HPos.LEFT);
		btnUpdate.setOnAction(event -> this.updateAction());
		btnUpdate.setDisable(true);

		Button btnCreate = new Button("Opret");
		pane.add(btnCreate, 1, 10);
		GridPane.setHalignment(btnCreate, HPos.LEFT);
		btnCreate.setOnAction(event -> this.createAction());

	}

	private Object createAction() {
		OpretKunde ok = new OpretKunde();
		ok.showAndWait();
		return null;
	}

	private Object updateAction() {
		OpdaterKunde ok = new OpdaterKunde();
		OpdaterKunde.updateControlsOpretKunde();
		ok.showAndWait();
		btnBekræft.setDisable(true);
		btnUpdate.setDisable(true);
		return null;
	}
	
	

//	// -------------------------------------------------------------------------

	public static ListView<Kunde> getLvwKunder() {
		return lvwKunder;
	}

	private Object selectedKunde() {
		btnBekræft.setDisable(false);
		btnUpdate.setDisable(false);
		return null;
	}

	public static void updateControlsFindKunde() {
		ArrayList<Kunde> alKunder = new ArrayList<>(Controller.getController().getKunder());
		Kunde.selectionSort(alKunder);
		lvwKunder.getItems().setAll(alKunder);
	}

	private Object cancelAction() {
		this.hide();
		return null;
	}

	private Object bekræftAction() {
		this.hide();
		return null;
	}

}
