package gui;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import controller.Controller;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Bestilling;
import model.Fadølsanlæg;
import model.Kunde;
import model.Transaktion;
import model.TransaktionsListeLinje;

public class BestillingWindow extends Stage {

	private Bestilling bestilling;
	private Controller controller = Controller.getController();

	public BestillingWindow(String title, Bestilling bestilling) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.bestilling = bestilling;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	public BestillingWindow(String title) {
		this(title, null);
	}

	private TextField txfId, txfStartDato, txfSlutDato, txfPris, txfBeskrivelse, txfAntalPersoner, txfTidspunkt;
	private Label lblError;
	private ComboBox<Kunde> cbox = null;

	private void initContent(GridPane pane) {

		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
//		pane.setGridLinesVisible(true);

		Label lblId = new Label("Id");
		pane.add(lblId, 0, 0);
		txfId = new TextField();
		pane.add(txfId, 1, 0);
		txfId.setPrefWidth(200);

		Label lblStartDato = new Label("Startdato (yyyy-MM-dd)");
		pane.add(lblStartDato, 0, 2);
		txfStartDato = new TextField();
		pane.add(txfStartDato, 1, 2);
		txfStartDato.setPrefWidth(200);

		Label lblSlutDato = new Label("SlutDato");
		pane.add(lblSlutDato, 0, 3);
		txfSlutDato = new TextField();
		pane.add(txfSlutDato, 1, 3);
		txfSlutDato.setPrefWidth(200);

		Label lblPris = new Label("Pris");
		pane.add(lblPris, 0, 4);
		txfPris = new TextField();
		pane.add(txfPris, 1, 4);
		txfPris.setPrefWidth(200);

		Label lblBeskrivelse = new Label("Beskrivelse");
		pane.add(lblBeskrivelse, 0, 5);
		txfBeskrivelse = new TextField();
		pane.add(txfBeskrivelse, 1, 5);
		txfBeskrivelse.setPrefWidth(200);

		Label lblAntalPersoner = new Label("Antal Personer");
		pane.add(lblAntalPersoner, 0, 6);
		txfAntalPersoner = new TextField();
		pane.add(txfAntalPersoner, 1, 6);
		txfAntalPersoner.setPrefWidth(200);

		Label lblTidspunkt = new Label("Tidspunkt (HH:mm)");
		pane.add(lblTidspunkt, 0, 7);
		txfTidspunkt = new TextField();
		pane.add(txfTidspunkt, 1, 7);
		txfTidspunkt.setPrefWidth(200);

		// ----------------------------

		cbox = new ComboBox<Kunde>();
		pane.add(cbox, 0, 9);
		cbox.getItems().addAll(controller.getKunder());

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 8);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		Button btnOK = new Button("OK");
		pane.add(btnOK, 1, 8);
		GridPane.setHalignment(btnOK, HPos.RIGHT);
		btnOK.setOnAction(event -> this.okAction());

		lblError = new Label();
		pane.add(lblError, 1, 9);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		if (bestilling != null) {
			txfId.setText(bestilling.getId());
			txfStartDato.setText("" + bestilling.getStartDato());
			txfSlutDato.setText("" + bestilling.getSlutDato());
			txfPris.setText(Double.toString(bestilling.getTll().getPris()));
			txfBeskrivelse.setText("" + bestilling.getBasicBeskrivelse());
			if (!bestilling.getBasicBeskrivelse().contains("Fadølsanlæg")) {
				txfAntalPersoner.setText(Integer.toString(bestilling.getAntalPersoner()));
				txfTidspunkt.setText("" + bestilling.getTidspunkt().format(DateTimeFormatter.ofPattern("HH:mm")));
			}

		} else {
			txfId.clear();
			txfStartDato.clear();
			txfSlutDato.clear();
			txfPris.clear();
			txfBeskrivelse.clear();
			txfAntalPersoner.clear();
			txfTidspunkt.clear();

		}
	}

	// -------------------------------------------------------------------------

	private void cancelAction() {
		this.hide();
	}

	private void okAction() {
		String id = txfId.getText().trim();

		if (id.length() == 0) {
			lblError.setText("Id er tomt");
			return;
		}
		for (Bestilling b : controller.getBestillinger()) {
			if (b.getId().equals(id)) {
				lblError.setText("Id allerede i brug");
				return;
			}
		}

		DateTimeFormatter localDate1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate startDato = LocalDate.parse(txfStartDato.getText(), localDate1);
		if (txfStartDato.getLength() == 0) {
			lblError.setText("Start dato er tomt");
			return;
		}

		LocalDate slutDato = LocalDate.parse(txfSlutDato.getText(), localDate1);
		if (txfSlutDato.getLength() == 0) {
			lblError.setText("Slut dato er tomt");
			return;
		}

		double pris = Double.parseDouble(txfPris.getText().trim());

		if (pris < 0) {
			lblError.setText("Pris er tomt");
			return;
		}
		String beskrivelse = txfBeskrivelse.getText().trim();

		if (beskrivelse.length() == 0) {
			lblError.setText("Beskrivelse er tomt");
			return;
		}

		int antalPersoner = -1;
		LocalTime tidspunkt = null;
		if (!beskrivelse.contains("Fadølsanlæg")) {
			antalPersoner = Integer.parseInt(txfAntalPersoner.getText().trim());

			if (antalPersoner < 0) {
				lblError.setText("antalPersoner");
				return;
			}

			DateTimeFormatter localTime1 = DateTimeFormatter.ofPattern("HH:mm");
			tidspunkt = LocalTime.parse(txfTidspunkt.getText(), localTime1);
			if (txfTidspunkt.getLength() == 0) {
				lblError.setText("Tidspunkt er tomt");
				return;
			}
		}
		if (cbox.getValue() == null) {
			lblError.setText("Kunde er ikke valgt");
			return;
		}

		// Call controller methods
		// update
		if (bestilling != null) {
			if (txfBeskrivelse.toString().contains("Fadølsanlæg")) {
				controller.updateBestilling(bestilling, id, startDato, slutDato);
				bestilling.getTll().setPris(pris);
				// tilføje flere fadølsanlæg
			} else {
				controller.updateBestilling(bestilling, id, startDato, slutDato, antalPersoner, tidspunkt);
				bestilling.getTll().setPris(pris);
			}

		} else {
			// create

			TransaktionsListeLinje tll = controller.createTransaktionsListeLinje(pris, 1);
			Transaktion t = controller.createTransaktion(LocalDate.now(), Integer.valueOf(id));
			Kunde k = cbox.getValue();
			// fadølsanlægsbestilling
			if (txfBeskrivelse.toString().contains("Fadølsanlæg")) {
				Bestilling b = controller.createFadølsanlægBestilling(id, startDato, slutDato);
				b.setTll(tll);
				for (Fadølsanlæg f : controller.getFadølsanlæg()) {
					if (f.getBeskrivelse().equals(beskrivelse)) {
						// TODO
						// add fadølsanlæg til bestilling
						b.addFadølsanlæg(f);
						// tilføje flere fadølsanlæg
					}
				}
			} else {
				// rundvisningbestilling
				Bestilling b = controller.createRundvisningBestilling(beskrivelse, id, startDato, slutDato,
						antalPersoner, tidspunkt);
				b.setTll(tll);

			}
			controller.addTransaktionsListeLinjeTilTransaktion(t, tll);
			k.addTransaktioner(t);

		}

		this.hide();
	}
}
