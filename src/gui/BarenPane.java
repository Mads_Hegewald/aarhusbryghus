package gui;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Bestilling;
import model.Betaling;
import model.Betalingstype;
import model.Kunde;
import model.PrisListe;
import model.Produkt;
import model.Rundvisning;
import model.Transaktion;
import model.TransaktionsListeLinje;

import java.time.LocalDate;
import java.util.ArrayList;
import controller.Controller;
import gui.Prislister;

public class BarenPane extends GridPane {
	private static TextField txfPris;
	private TextField txfKunde;
	private Double prisValue = 0.0;
	private TextField txfPrisliste;
	private TextField txfBetales;
	private ArrayList<Produkt> alP = new ArrayList<>();
	private ArrayList<Produkt> alPl = new ArrayList<>();
	private ArrayList<Betaling> alBet = new ArrayList<>();
	private ListView<String> lvwProdukter;
	private ListView<String> lvwIndkøbskurv;
	private ComboBox<Betalingstype> eCBox = null;
	private Button btnBetal = new Button("Betal");
	Button btnTilføj = new Button("Tilføj produkt");
	Button btnFjern = new Button("Fjern produkt");
	Button btnForfra = new Button("Ryd kunde");
	Button btnBestilling = new Button("Bestillinger");
	Label lblError = new Label();
	Boolean fadølsanlæg = false;
	Boolean bestilling = false;
	Bestilling chosenBestilling;
	Produkt f = null;
	Produkt f1 = null;

	public BarenPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		Label lblBaren = new Label("Barsalg");
		this.add(lblBaren, 0, 3);

		lvwProdukter = new ListView<>();
		this.add(lvwProdukter, 0, 4, 1, 5);
		lvwProdukter.setPrefWidth(200);
		lvwProdukter.setPrefHeight(200);

		ChangeListener<String> listenerProdukter = (ov, oldProdukter, newProdukter) -> this.selectedProduktChanged();
		lvwProdukter.getSelectionModel().selectedItemProperty().addListener(listenerProdukter);

		btnTilføj.setOnAction(event -> this.tilføjAction());
		this.add(btnTilføj, 1, 5);
		btnTilføj.setDisable(true);

		btnFjern.setOnAction(event -> this.fjernAction());
		this.add(btnFjern, 1, 6);
		btnFjern.setDisable(true);

		Label lblIK = new Label("Indkøbskurv");
		this.add(lblIK, 2, 3);

		lvwIndkøbskurv = new ListView<>();
		this.add(lvwIndkøbskurv, 2, 4, 1, 5);
		lvwIndkøbskurv.setPrefWidth(200);
		lvwIndkøbskurv.setPrefHeight(200);

		ChangeListener<String> listenerIndkøbskurv = (ov, oldIndkøbskurv, newIndkøbskurv) -> this
				.selectedIndkøbskurvChanged();
		lvwIndkøbskurv.getSelectionModel().selectedItemProperty().addListener(listenerIndkøbskurv);

		txfPrisliste = new TextField();
		this.add(txfPrisliste, 2, 1);
		txfPrisliste.setEditable(false);

		Button btnKunde = new Button("Søg/opret kunde");
		btnKunde.setOnAction(event -> this.opretKunde());
		this.add(btnKunde, 0, 0);

		btnForfra.setOnAction(event -> this.rydKundeAction());
		this.add(btnForfra, 1, 0);

		txfKunde = new TextField();
		this.add(txfKunde, 0, 1);
		txfKunde.setEditable(false);

		Button btnPrisliste = new Button("Vælg prisListe");
		btnPrisliste.setOnAction(event -> this.prisListeAction());
		this.add(btnPrisliste, 2, 0);

		btnBetal.setOnAction(event -> this.betalAction());
		this.add(btnBetal, 4, 8);
		btnBetal.setDisable(true);

		eCBox = new ComboBox<Betalingstype>();
		this.add(eCBox, 4, 5);
		eCBox.setOnAction(event -> this.boxAction());
		Label lblBetalingsmetode = new Label("Betalingsmetode");
		this.add(lblBetalingsmetode, 3, 5);

		btnBestilling.setOnAction(event -> this.bestillingAction());
		this.add(btnBestilling, 3, 0);

		Label lblBeløb = new Label("Betales:");
		this.add(lblBeløb, 3, 3);
		txfBetales = new TextField();
		this.add(txfBetales, 4, 3);

		Label lblPris = new Label("Pris:");
		this.add(lblPris, 3, 7);

		txfPris = new TextField();
		this.add(txfPris, 4, 7);
		txfPris.setEditable(false);

		this.add(lblError, 0, 11);
		lblError.setStyle("-fx-text-fill: red");

		initControls();

		// -------------------------------------------------------------------------
	}

	private Object bestillingAction() {
		txfPrisliste.clear();
		txfPris.clear();
		alP.clear();
		alPl.clear();
		lvwProdukter.getItems().clear();
		lvwIndkøbskurv.getItems().clear();
		prisValue = 0.0;
		BarBestilling bb = new BarBestilling();
		bb.showAndWait();
		chosenBestilling = BarBestilling.getLvwBestillinger().getSelectionModel().getSelectedItem();

		if (chosenBestilling != null) {
			bestilling = true;
			txfKunde.setText(getKundeTilBestilling().getNavn());
			lvwIndkøbskurv.getItems().add(chosenBestilling.getBasicBeskrivelse());
			alPl.add(chosenBestilling);
			btnTilføj.setDisable(true);
			prisValue = chosenBestilling.getTll().getPris();
			txfPris.setText(prisValue.toString());
			btnForfra.setDisable(true);
			if (chosenBestilling.getBasicBeskrivelse().contains("Fadølsanlæg")) {
				fadølsanlæg = true;
				f = Controller.getController().createProdukt("Brugt fustage", 200.0, 25);
				f1 = Controller.getController().createProdukt("Ubrugt fustage", 200.0, 25);
				alP.add(f);
				alP.add(f1);
				alP.get(0).addPll(Controller.getController().createPrisListeLinje(200.0, "Butik"));
				alP.get(1).addPll(Controller.getController().createPrisListeLinje(200.0, "Butik"));
				for (PrisListe pl : Controller.getController().getPrisListe()) {
					if (pl.getBeskrivelse().equals("Butik")) {
						pl.addPrisListeLinje(alP.get(0).getPrisListeLinje().get(0));
						pl.addPrisListeLinje(alP.get(1).getPrisListeLinje().get(0));
					}
				}
				alPl.add(alP.get(0));
				alPl.add(alP.get(1));
			}
			for (Produkt p : alP) {
				lvwProdukter.getItems().add(p.getBeskrivelse());
			}
			alP.add(chosenBestilling);
			alPl.add(chosenBestilling);
			lvwProdukter.getItems().add(chosenBestilling.getBasicBeskrivelse());
			txfPrisliste.setText("Butik");
		}
		return null;
	}

	private Kunde getKundeTilBestilling() {
		Bestilling bes = BarBestilling.getLvwBestillinger().getSelectionModel().getSelectedItem();
		Kunde kun = null;
		for (int i = 0; i < Controller.getController().getKunder().size(); i++) {
			for (int k = 0; k < Controller.getController().getKunder().get(i).getTransaktioner().size(); k++) {
				for (int j = 0; j < Controller.getController().getKunder().get(i).getTransaktioner().get(k)
						.getTransaktionsListeLinje().size(); j++) {
					if (Controller.getController().getKunder().get(i).getTransaktioner().get(k)
							.getTransaktionsListeLinje().get(j).getProdukt() == bes) {
						kun = Controller.getController().getKunder().get(i);

					}

				}

			}

		}
		return kun;
	}

	private Object rydKundeAction() {
		txfKunde.clear();
		return null;
	}

	private Object opretKunde() {
		FindKunde ok = new FindKunde();
		ok.showAndWait();
		if (FindKunde.getLvwKunder().getSelectionModel().getSelectedItem() != null) {
			updateControlsBarKunde();
		}
		return null;
	}

	private Object boxAction() {
		btnBetal.setDisable(false);
		return null;
	}

	private Object betalAction() {
		Betaling beta = new Betaling(Double.parseDouble(txfBetales.getText()), eCBox.getValue());
		alBet.add(beta);
		prisValue -= Double.valueOf(txfBetales.getText());
		eCBox.setValue(null);
		txfPris.setText(prisValue.toString());
		btnBetal.setDisable(true);
		btnForfra.setDisable(false);

		if (Double.parseDouble(txfPris.getText()) < 0.0) {
			Byttepenge bp = new Byttepenge(prisValue);
			bp.showAndWait();
			txfPris.setText("0.0");
			prisValue = 0.0;
			fadølsanlæg = false;
		}

		if (txfPris.getText().equals("0.0") && bestilling == false) {
			Transaktion t;
			t = Controller.getController().createTransaktion(LocalDate.now(),
					Controller.getController().getTransaktion().size() + 1);

			for (int i = 0; i < alP.size(); i++) {
				for (int j = 0; j < alP.get(i).getPrisListeLinje().size(); j++) {
					if (alP.get(i).getPrisListeLinje().get(j).getBeskrivelse().equals(txfPrisliste.getText())) {
						TransaktionsListeLinje tll = Controller.getController()
								.createTransaktionsListeLinje(alP.get(i).getPrisListeLinje().get(j).getPris(), 1);
						t.addTll(tll);
						for (Betaling betaling : alBet) {
							t.addBetalinger(betaling);
						}
						alBet.clear();
						alP.get(i).setTllNull();
						alP.get(i).setTll(tll);
					}
				}
			}

			if (!txfKunde.getText().isEmpty()) {

				Kunde k = null;
				for (int i = 0; i < Controller.getController().getKunder().size(); i++) {
					if (txfKunde.getText().trim().equals(Controller.getController().getKunder().get(i).getNavn()))
						k = Controller.getController().getKunder().get(i);
					t.setKunde(k);
				}
			}
			txfBetales.clear();
			lvwIndkøbskurv.getItems().clear();
			alP.clear();
		}

		if (txfPris.getText().equals("0.0") && bestilling == true) {
			for (int i = 0; i < Controller.getController().getTransaktion().size(); i++) {
				for (int j = 0; j < Controller.getController().getTransaktion().get(i).getTransaktionsListeLinje()
						.size(); j++) {
					if (Controller.getController().getTransaktion().get(i).getTransaktionsListeLinje().get(j)
							.getProdukt() == chosenBestilling) {
						for (Betaling betaling : alBet) {
							Controller.getController().getTransaktion().get(i).addBetalinger(betaling);
						}
						alBet.clear();
					}
				}
			}

			if (!(chosenBestilling instanceof Rundvisning) == true) {
				Controller.getController().removeProdukt(f);
				Controller.getController().removeProdukt(f1);
			}
			Controller.getController().removeBestilling(chosenBestilling);
			lvwProdukter.getItems().clear();
		}

		txfBetales.clear();
		lvwIndkøbskurv.getItems().clear();
		alP.clear();
		lblError.setText("");
		return null;
	}

	public ListView<String> getLvwProdukter() {
		return lvwProdukter;
	}

	public ListView<String> getLvwIndkøbskurv() {
		return lvwIndkøbskurv;
	}

	private void fjernAction() {
		if (lvwIndkøbskurv.getSelectionModel().getSelectedItem().contains("Fadølsanlæg")
				|| lvwIndkøbskurv.getSelectionModel().getSelectedItem().contains("Rundvisning")) {
			lblError.setText("Bestilling kan ikke fjernes fra kurven");
			return;
		} else {

			if (alP.size() <= 1) {
				alP.clear();
				lvwIndkøbskurv.getItems().clear();
				txfPris.clear();
				btnFjern.setDisable(true);
				prisValue = 0.0;
				return;
			}

			for (int i = 0; i < alP.get(lvwIndkøbskurv.getSelectionModel().getSelectedIndex()).getPrisListeLinje()
					.size(); i++) {
				if (alP.get(lvwIndkøbskurv.getSelectionModel().getSelectedIndex()).getPrisListeLinje().get(i)
						.getBeskrivelse().equals(txfPrisliste.getText())) {
					prisValue -= alP.get(lvwIndkøbskurv.getSelectionModel().getSelectedIndex()).getPrisListeLinje()
							.get(0).getPris();
				}
			}
			alP.remove(lvwIndkøbskurv.getSelectionModel().getSelectedIndex());
			lvwIndkøbskurv.getItems().remove(lvwIndkøbskurv.getSelectionModel().getSelectedIndex());

			txfPris.setText(prisValue.toString());
		}
	}

	private void tilføjAction() {
		if (lvwProdukter.getSelectionModel().getSelectedItem().contains("Fadølsanlæg")
				|| lvwProdukter.getSelectionModel().getSelectedItem().contains("Rundvisning")) {
			lblError.setText("Bestilling er tilføjet til kurven");
			return;
		} else {
			if (fadølsanlæg == true) {
				lvwIndkøbskurv.getItems().remove(lvwIndkøbskurv.getItems().get(lvwIndkøbskurv.getItems().size() - 1));
				alPl.remove(chosenBestilling);

				lvwIndkøbskurv.getItems().add(lvwProdukter.getSelectionModel().getSelectedItem());
				alP.add(alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()));
				for (int j = 0; j < alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()).getPrisListeLinje()
						.size(); j++) {
					if (alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()).getBeskrivelse()
							.equals("Brugt fustage")) {

						prisValue += alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()).getPrisListeLinje()
								.get(0).getPris();
						prisValue -= alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()).getPant();
						lvwIndkøbskurv.getItems().add(chosenBestilling.getBasicBeskrivelse());
						alPl.add(chosenBestilling);
						txfPris.setText(prisValue.toString());
					} else {
						if (alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()).getBeskrivelse()
								.equals("Ubrugt fustage")) {
							prisValue -= alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()).getPant();
							lvwIndkøbskurv.getItems().add(chosenBestilling.getBasicBeskrivelse());
							alPl.add(chosenBestilling);
							txfPris.setText(prisValue.toString());
						}
					}
				}
			} else {
				lvwIndkøbskurv.getItems().add(lvwProdukter.getSelectionModel().getSelectedItem());
				alP.add(alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()));
				for (int i = 0; i < alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()).getPrisListeLinje()
						.size(); i++) {
					if (alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()).getPrisListeLinje().get(i)
							.getBeskrivelse().equals(txfPrisliste.getText())) {
						prisValue += alPl.get(lvwProdukter.getSelectionModel().getSelectedIndex()).getPrisListeLinje()
								.get(0).getPris();
					}
				}
				txfPris.setText(prisValue.toString());
			}
		}
	}

	private Object selectedIndkøbskurvChanged() {
		btnFjern.setDisable(false);
		return null;
	}

	private void sorterLister() {
		Produkt.selectionSort(alP);
		Produkt.selectionSort(alPl);
	}

//	// ----------------------------INIT----------------------------------------

	public void initControls() {
		txfPrisliste.setText(Controller.getController().getPrisListe().get(0).getBeskrivelse());

		for (int i = 0; i < Controller.getController().getPrisListe().size(); i++) {
			if (Controller.getController().getPrisListe().get(i).getBeskrivelse().equals(txfPrisliste.getText()))
				for (int j = 0; j < Controller.getController().getPrisListe().get(i).getPrisListeLinje().size(); j++) {
					alPl.add(Controller.getController().getPrisListe().get(i).getPrisListeLinje().get(j).getProdukt());
				}
		}
		ArrayList<String> s = new ArrayList<>();
		for (int i = 0; i < Controller.getController().getProdukt().size(); i++) {
			String temp;
			String tempPris = "Null";
			for (int j = 0; j < Controller.getController().getProdukt().get(i).getPrisListeLinje().size(); j++) {
				if (Controller.getController().getProdukt().get(i).getPrisListeLinje().get(j).getBeskrivelse()
						.equals(txfPrisliste.getText())) {

					tempPris = Controller.getController().getProdukt().get(i).getPrisListeLinje().get(j).getPris()
							.toString();
					temp = Controller.getController().getProdukt().get(i).getBeskrivelse() + " " + tempPris;
					s.add(temp);
				}
			}
		}
		lvwProdukter.getItems().setAll(s);
		eCBox.getItems().add(Betalingstype.DANKORT);
		eCBox.getItems().add(Betalingstype.KLIPPEKORT);
		eCBox.getItems().add(Betalingstype.KONTANT);
		eCBox.getItems().add(Betalingstype.MOBILEPAY);
	}

	public void updateControlsBarKunde() {
		txfKunde.setText(FindKunde.getLvwKunder().getSelectionModel().getSelectedItem().getNavn());
	}

	public void updateControlsBar() {
		txfPris.clear();
		alP.clear();
		alPl.clear();
		txfPrisliste.setText(Prislister.getLvwPrisliste().getSelectionModel().getSelectedItem().getBeskrivelse());

		ArrayList<String> s = new ArrayList<>();
		String temp;
		String tempPris = "b";

		for (int i = 0; i < Controller.getController().getPrisListe().size(); i++) {
			if (Controller.getController().getPrisListe().get(i).getBeskrivelse().equals(txfPrisliste.getText()))
				for (int j = 0; j < Controller.getController().getPrisListe().get(i).getPrisListeLinje().size(); j++) {
					alPl.add(Controller.getController().getPrisListe().get(i).getPrisListeLinje().get(j).getProdukt());

					tempPris = Controller.getController().getPrisListe().get(i).getPrisListeLinje().get(j).getPris()
							.toString();
					temp = Controller.getController().getPrisListe().get(i).getPrisListeLinje().get(j).getProdukt()
							.getBeskrivelse() + " " + tempPris;
					s.add(temp);
				}
		}
		prisValue = 0.00;
		lvwIndkøbskurv.getItems().clear();
		lvwProdukter.getItems().setAll(s);
	}

	// // -------------------------------------------------------------------------

	private void selectedProduktChanged() {
		btnTilføj.setDisable(false);
	}

	private void prisListeAction() {
		fadølsanlæg = false;
		Prislister pl = new Prislister();
		pl.showAndWait();
		if (Prislister.getLvwPrisliste().getSelectionModel().getSelectedItem() != null) {
			updateControlsBar();
			btnForfra.setDisable(false);
			btnTilføj.setDisable(false);
			txfKunde.clear();
		}
	}
}
