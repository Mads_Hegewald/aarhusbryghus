package gui;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Fustage;
import model.Klippekort;
import model.PrisListe;
import model.PrisListeLinje;
import model.Produkt;

public class ProduktWindow extends Stage {

	private Produkt produkt;
	private PrisListe prisListe;
	private Controller controller = Controller.getController();
	Button btnOK = new Button("OK");

	public ProduktWindow(String title, Produkt produkt, PrisListe prisListe) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.produkt = produkt;
		this.prisListe = prisListe;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	public ProduktWindow(String title, PrisListe prisListe) {
		this(title, null, prisListe);
	}

	private TextField txfBeskrivelse, txfAntalKlip, txfPant, txfLiter, txfPris;
	private Label lblError;
	private ComboBox<String> cbox = null;

	private void initContent(GridPane pane) {

		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
//		pane.setGridLinesVisible(true);

		Label lblBeskrivelse = new Label("Beskrivelse");
		pane.add(lblBeskrivelse, 0, 1);
		txfBeskrivelse = new TextField();
		pane.add(txfBeskrivelse, 1, 1);
		txfBeskrivelse.setPrefWidth(200);

		Label lblAntalKlip = new Label("AntalKlip");
		pane.add(lblAntalKlip, 0, 2);
		txfAntalKlip = new TextField();
		pane.add(txfAntalKlip, 1, 2);
		txfAntalKlip.setPrefWidth(200);

		Label lblPant = new Label("Pant");
		pane.add(lblPant, 0, 3);
		txfPant = new TextField();
		pane.add(txfPant, 1, 3);
		txfPant.setPrefWidth(200);

		Label lblLiter = new Label("Liter");
		pane.add(lblLiter, 0, 4);
		txfLiter = new TextField();
		pane.add(txfLiter, 1, 4);
		txfLiter.setPrefWidth(200);

		Label lblPris = new Label("Pris");
		pane.add(lblPris, 0, 5);
		txfPris = new TextField();
		pane.add(txfPris, 1, 5);
		txfPris.setPrefWidth(200);

		// ----------------------------

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 7);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		pane.add(btnOK, 1, 7);
		GridPane.setHalignment(btnOK, HPos.RIGHT);
		btnOK.setOnAction(event -> this.okAction());
		btnOK.setDisable(true);

		lblError = new Label();
		pane.add(lblError, 1, 8);
		lblError.setStyle("-fx-text-fill: red");

		// combobox med produkttyper
		Label lblProduktValg = new Label("Produkt type");
		pane.add(lblProduktValg, 0, 0);
		cbox = new ComboBox<String>();
		cbox.getItems().add("Øl");
		cbox.getItems().add("Klippekort");
		cbox.getItems().add("Fustage");
		pane.add(cbox, 1, 0);

		ChangeListener<String> listener = (ov, oldString, newString) -> this.selectedProduktChanged();
		cbox.getSelectionModel().selectedItemProperty().addListener(listener);
		cbox.getSelectionModel().clearSelection();

		this.initControls();
	}

	private void initControls() {
		if (produkt != null) {
			txfBeskrivelse.setText("" + produkt.getBeskrivelse());
			for (int i = 0; i < produkt.getPrisListeLinje().size(); i++) {
				if (produkt.getPrisListeLinje().get(i).getBeskrivelse().equals(prisListe.getBeskrivelse())) {
					txfPris.setText("" + produkt.getPrisListeLinje().get(i).getPris());
				}
			}

			if (produkt instanceof Klippekort) {
				txfAntalKlip.setText("" + Integer.toString(produkt.getAntalKlip()));
			}
			if (produkt instanceof Fustage) {
				txfPant.setText("" + Double.toString(produkt.getPant()));
				txfLiter.setText("" + Integer.toString(produkt.getLiter()));
			}

		} else {
			txfBeskrivelse.clear();
			txfPris.clear();
			txfAntalKlip.clear();
			txfPant.clear();
			txfLiter.clear();
		}
	}

	// -------------------------------------------------------------------------

	private void cancelAction() {
		this.hide();
	}

	private void selectedProduktChanged() {
		updateControls();
	}

	private void updateControls() {
		if (cbox.getValue().equals("Øl")) {
			txfAntalKlip.setDisable(true);
			txfPant.setDisable(true);
			txfLiter.setDisable(true);
		} else if (cbox.getValue().equals("Klippekort")) {
			txfPant.setDisable(true);
			txfLiter.setDisable(true);
			txfAntalKlip.setDisable(false);
		} else {
			txfAntalKlip.setDisable(true);
			txfPant.setDisable(false);
			txfLiter.setDisable(false);
		}
		btnOK.setDisable(false);
	}

	private void okAction() {

		if (cbox.getValue() == null) {
			lblError.setText("Produkt er ikke valgt");
		}

		String beskrivelse = txfBeskrivelse.getText().trim();

		if (beskrivelse.length() == 0) {
			lblError.setText("Beskrivelse er tomt");
			return;
		}
		for (Produkt b : controller.getProdukt()) {
			if (b.getBeskrivelse().equals(beskrivelse)) {
				lblError.setText("Beskrivelse allerede i brug");
				return;
			}
		}

		double pris = Double.parseDouble(txfPris.getText().trim());

		if (pris < 0) {
			lblError.setText("Pris er tomt");
			return;
		}

		// hvis klippekort
		int antalKlip = -1;
		if (cbox.getValue().equals("Klippekort")) {
			antalKlip = Integer.parseInt(txfAntalKlip.getText().trim());

			if (antalKlip < 0) {
				lblError.setText("Antal Klip er tomt");
				return;
			}
		}

		// hvis fustage
		double pant = -1;
		int liter = -1;
		if (cbox.getValue().equals("Fustage")) {
			pant = Double.parseDouble(txfPant.getText().trim());

			if (pant < 0) {
				lblError.setText("Pant er tomt");
				return;
			}

			liter = Integer.parseInt(txfLiter.getText().trim());

			if (liter < 0) {
				lblError.setText("Liter er tomt");
				return;
			}
		}

		// Call controller methods
		// update
		if (produkt != null) {
			if (cbox.getValue().equals("Øl")) {
				// øl
				controller.updateProdukt(produkt, beskrivelse);

			} else if (cbox.getValue().equals("Klippekort")) {
				// klippekort
				controller.updateProdukt(produkt, beskrivelse, antalKlip);
			} else {
				// fustage
				controller.updateProdukt(produkt, beskrivelse, pant, liter);
			}
			for (int i = 0; i < produkt.getPrisListeLinje().size(); i++) {
				if (produkt.getPrisListeLinje().get(i).getPris() != pris) {
					produkt.getPrisListeLinje().get(i).setPris(pris);
				}
			}

		} else {
			// create
			PrisListeLinje pll = controller.createPrisListeLinje(pris, prisListe.getBeskrivelse());
			if (cbox.getValue().equals("Øl")) {
				// øl
				Produkt p = controller.createProdukt(beskrivelse);
				p.addPll(pll);
			} else if (cbox.getValue().equals("Klippekort")) {
				// klippekort
				Produkt p = controller.createProdukt(beskrivelse, antalKlip);
				p.addPll(pll);
			} else {
				// fustage
				Produkt p = controller.createProdukt(beskrivelse, pant, liter);
				p.addPll(pll);
			}
			controller.addPrisListeLinjeTilPrisListe(prisListe, pll);

		}

		this.hide();
	}

}
