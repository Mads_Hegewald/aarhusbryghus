package gui;

import controller.Controller;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Kunde;

public class OpretKunde extends Stage {

	private Controller controller = Controller.getController();
	private static TextField txfNavn;
	private static TextField txfAdresse;
	private static TextField txfTlf;
	private static TextField txfCvr;
	private Label lblError;

	public OpretKunde() {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle("Opret kunde");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	private void initContent(GridPane pane) {

		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblNavn = new Label("Navn");
		pane.add(lblNavn, 0, 0);
		txfNavn = new TextField();
		pane.add(txfNavn, 1, 0);
		txfNavn.setPrefWidth(200);

		Label lblAdresse = new Label("Adresse");
		pane.add(lblAdresse, 0, 2);
		txfAdresse = new TextField();
		pane.add(txfAdresse, 1, 2);
		txfAdresse.setPrefWidth(200);

		Label lblTlf = new Label("Telefon nummer");
		pane.add(lblTlf, 0, 3);
		txfTlf = new TextField();
		pane.add(txfTlf, 1, 3);
		txfTlf.setPrefWidth(200);

		Label lblVirksomhed = new Label("Virksomhed");
		pane.add(lblVirksomhed, 0, 4);
		txfCvr = new TextField();
		pane.add(txfCvr, 1, 4);
		txfCvr.setPrefWidth(200);

		// ----------------------------

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 7);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		Button btnOK = new Button("Ok");
		pane.add(btnOK, 1, 7);
		GridPane.setHalignment(btnOK, HPos.RIGHT);
		btnOK.setOnAction(event -> this.okAction());

		lblError = new Label();
		pane.add(lblError, 1, 8);
		lblError.setStyle("-fx-text-fill: red");

	}

	// -------------------------------------------------------------------------

	private void cancelAction() {
		this.hide();
	}

	private void okAction() {

		String beskrivelse = txfNavn.getText().trim();

		if (beskrivelse.length() == 0) {
			lblError.setText("Navn er tom");
			return;
		}
		for (Kunde b : controller.getKunder()) {
			if (b.getNavn().equals(beskrivelse)) {
				lblError.setText("Kunde allerede oprettet");
				return;
			}
		}

		int tlf = Integer.parseInt(txfTlf.getText().trim());

		if (txfTlf.getText().length() == 0) {
			lblError.setText("Telefon nummer er tom");
			return;
		}
		for (Kunde b : controller.getKunder()) {
			if (b.getTlfnr() == tlf) {
				lblError.setText("Telefon nummer allerede oprettet");
				return;
			}
		}
		controller.createKunde(txfNavn.getText(), txfAdresse.getText(), Integer.parseInt(txfTlf.getText()));

		FindKunde.updateControlsFindKunde();
		this.hide();
	}

	public static void updateControlsOpretKunde() {
		txfNavn.setText(FindKunde.getLvwKunder().getSelectionModel().getSelectedItem().getNavn());
		txfAdresse.setText(FindKunde.getLvwKunder().getSelectionModel().getSelectedItem().getAdresse());
		txfTlf.setText(Integer.toString(FindKunde.getLvwKunder().getSelectionModel().getSelectedItem().getTlfnr()));
		if (FindKunde.getLvwKunder().getSelectionModel().getSelectedItem().getCvrNr() != -1) {
			txfCvr.setText(Integer.toString(FindKunde.getLvwKunder().getSelectionModel().getSelectedItem().getCvrNr()));
		}
	}

}
