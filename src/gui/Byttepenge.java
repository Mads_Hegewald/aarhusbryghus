package gui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Byttepenge extends Stage {

	private Label lblError;
	Double value= 0.0;
	
	public Byttepenge(Double value) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle("Byttepenge");
		GridPane pane = new GridPane();
		this.initContent(pane, value);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	private void initContent(GridPane pane, Double value) {

		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblByttepenge = new Label("Byttepenge");
		pane.add(lblByttepenge, 0, 0);

		Label lblText = new Label("Udbetal " + (value*-1) + " til kunden");
		pane.add(lblText, 0, 2);

		// ----------------------------
		Button btnOK = new Button("Ok");
		pane.add(btnOK, 1, 7);
		GridPane.setHalignment(btnOK, HPos.RIGHT);
		btnOK.setOnAction(event -> this.okAction());

		lblError = new Label();
		pane.add(lblError, 1, 8);
		lblError.setStyle("-fx-text-fill: red");

	}

	// -------------------------------------------------------------------------

	private void okAction() {

		this.hide();
	}

	public static void updateControlsOpretKunde() {
	}
}
