package gui;

import controller.Controller;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.PrisListe;

public class PrisListeWindow extends Stage {

	private PrisListe prisListe;
	private Controller controller = Controller.getController();

	public PrisListeWindow(String title, PrisListe prisListe) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.prisListe = prisListe;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	public PrisListeWindow(String title) {
		this(title, null);
	}

	private TextField txfBeskrivelse;
	private Label lblError;

	private void initContent(GridPane pane) {

		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblBeskrivelse = new Label("Beskrivelse");
		pane.add(lblBeskrivelse, 0, 0);
		txfBeskrivelse = new TextField();
		pane.add(txfBeskrivelse, 1, 0);
		txfBeskrivelse.setPrefWidth(200);

		// ----------------------------

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 1);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		Button btnOK = new Button("OK");
		pane.add(btnOK, 1, 1);
		GridPane.setHalignment(btnOK, HPos.RIGHT);
		btnOK.setOnAction(event -> this.okAction());

		lblError = new Label();
		pane.add(lblError, 1, 2);
		lblError.setStyle("-fx-text-fill: red");

		this.initControls();
	}

	private void initControls() {
		if (prisListe != null) {
			txfBeskrivelse.setText("" + prisListe.getBeskrivelse());
		} else {
			txfBeskrivelse.clear();
		}
	}

	// -------------------------------------------------------------------------

	private void cancelAction() {
		this.hide();
	}

	private void okAction() {
		String beskrivelse = txfBeskrivelse.getText().trim();

		if (beskrivelse.length() == 0) {
			lblError.setText("Beskrivelse er tomt");
			return;
		}
		if (prisListe != null) {
			// update
			for (int i = 0; i < prisListe.getPrisListeLinje().size(); i++) {
				prisListe.getPrisListeLinje().get(i).setBeskrivelse(beskrivelse);
			}
			controller.updatePrisListe(prisListe, beskrivelse);

		} else {
			// create
			controller.createPrisListe(beskrivelse);
		}

		this.hide();
	}
}
