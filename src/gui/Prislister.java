package gui;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.PrisListe;

public class Prislister extends Stage {

	private static ListView<PrisListe> lvwPrisListe;
	Button btnBekræft = new Button("Bekræft");

	public Prislister() {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.setTitle("Prislister");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);

		Label lblPrislister = new Label("Prislister");
		pane.add(lblPrislister, 0, 0);

		lvwPrisListe = new ListView<>();
		pane.add(lvwPrisListe, 0, 4, 1, 5);
		lvwPrisListe.setPrefWidth(200);
		lvwPrisListe.setPrefHeight(200);

		ChangeListener<PrisListe> listenerPrisListe = (ov, oldPrisListe, newPrisListe) -> this.selectedPrisListe();
		lvwPrisListe.getSelectionModel().selectedItemProperty().addListener(listenerPrisListe);

		initControls();
	}

	// -------------------------------------------------------------------------

	private void initControls() {
		lvwPrisListe.getItems().setAll(Controller.getController().getPrisListe());

	}

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		Button btnCancel = new Button("Cancel");
		pane.add(btnCancel, 0, 9);
		GridPane.setHalignment(btnCancel, HPos.LEFT);
		btnCancel.setOnAction(event -> this.cancelAction());

		pane.add(btnBekræft, 0, 9);
		GridPane.setHalignment(btnBekræft, HPos.RIGHT);
		btnBekræft.setOnAction(event -> this.bekræftAction());
		btnBekræft.setDisable(true);

	}

//	// -------------------------------------------------------------------------

	public static ListView<PrisListe> getLvwPrisliste() {
		return lvwPrisListe;
	}

	private Object selectedPrisListe() {
		this.updateControls();
		btnBekræft.setDisable(false);
		return null;
	}

	private void updateControls() {
	}

	private Object cancelAction() {
		this.hide();
		return null;
	}

	private Object bekræftAction() {
		this.hide();
		return null;
	}

}
