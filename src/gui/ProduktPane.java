package gui;

import java.util.ArrayList;

import controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import model.PrisListe;
import model.Produkt;

public class ProduktPane extends GridPane {

	private ListView<Produkt> lvwProdukter;
	private ListView<PrisListe> lvwPrisLister;

	public ProduktPane() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		Label lblProdukter = new Label("Produkter");
		this.add(lblProdukter, 0, 0);

		Label lblPrisLister = new Label("Prislister");
		this.add(lblPrisLister, 3, 0);

		lvwPrisLister = new ListView<>();
		this.add(lvwPrisLister, 3, 1);
		lvwPrisLister.setPrefSize(150, 200);
		lvwPrisLister.getItems().setAll(Controller.getController().getPrisListe());

		ChangeListener<PrisListe> listener = (ov, oldPrisListe, newPrisListe) -> this.selectedPrisListeChanged();
		lvwPrisLister.getSelectionModel().selectedItemProperty().addListener(listener);
		lvwPrisLister.getSelectionModel().clearSelection();

		lvwProdukter = new ListView<>();
		this.add(lvwProdukter, 0, 1, 1, 3);
		lvwProdukter.setPrefSize(150, 200);

//		lvwProdukter = new ListView<>();
//		this.add(lvwProdukter, 0, 1, 1, 3);
//		lvwProdukter.setPrefSize(150, 200);
//		lvwProdukter.getItems().setAll(Controller.getController().getProdukt());

		// Produkt buttons
		Button btnOpretProdukt = new Button("Opret Produkt");
		this.add(btnOpretProdukt, 1, 1);
		GridPane.setValignment(btnOpretProdukt, VPos.TOP);
		btnOpretProdukt.setOnAction(event -> this.createProduktAction());

		Button btnUpdateProdukt = new Button("Opdater Produkt");
		this.add(btnUpdateProdukt, 1, 1);
		GridPane.setValignment(btnUpdateProdukt, VPos.BOTTOM);
		btnUpdateProdukt.setOnAction(event -> this.updateProduktAction());

		Button btnSletProdukt = new Button("Slet Produkt");
		this.add(btnSletProdukt, 1, 1);
		btnSletProdukt.setOnAction(event -> this.deleteProduktAction());

		// PrisListe buttons
		Button btnOpretPrisListe = new Button("Opret Prisliste");
		this.add(btnOpretPrisListe, 4, 1);
		GridPane.setValignment(btnOpretPrisListe, VPos.TOP);
		btnOpretPrisListe.setOnAction(event -> this.createPrisListeAction());

		Button btnUpdatePrisListe = new Button("Opdater Prisliste");
		this.add(btnUpdatePrisListe, 4, 1);
		GridPane.setValignment(btnUpdatePrisListe, VPos.BOTTOM);
		btnUpdatePrisListe.setOnAction(event -> this.updatePrisListeAction());

		Button btnSletPrisListe = new Button("Slet Prisliste");
		this.add(btnSletPrisListe, 4, 1);
		btnSletPrisListe.setOnAction(event -> this.deletePrisListeAction());

	}

	// ------------------------------Buttons----------------------------

	private void selectedPrisListeChanged() {
		this.updateControls();
	}

	private void updateControls() {
		if (lvwPrisLister.getSelectionModel().getSelectedItem() == null) {
			return;
		}
		PrisListe prisListe = lvwPrisLister.getSelectionModel().getSelectedItem();
		ArrayList<Produkt> produkter = new ArrayList<>();
		for (int i = 0; i < Controller.getController().getProdukt().size(); i++) {
			for (int j = 0; j < Controller.getController().getProdukt().get(i).getPrisListeLinje().size(); j++) {
				if (prisListe.getBeskrivelse().equals(
						Controller.getController().getProdukt().get(i).getPrisListeLinje().get(j).getBeskrivelse())) {
					produkter.add(Controller.getController().getProdukt().get(i));
				}
			}

		}
		lvwProdukter.getItems().setAll(produkter);
	}

	// Produkt buttons
	private void createProduktAction() {
		PrisListe prisListe = lvwPrisLister.getSelectionModel().getSelectedItem();
		if (prisListe == null) {
			return;
		}
		ProduktWindow dia = new ProduktWindow("Opret Produkt", prisListe);
		dia.showAndWait();

		// Wait for the modal dialog to close

		lvwProdukter.getItems().setAll(Controller.getController().getProdukt());
		int index = lvwProdukter.getItems().size() - 1;
		lvwProdukter.getSelectionModel().select(index);
		this.updateControls();
	}

	private void updateProduktAction() {
		Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
		PrisListe prisListe = lvwPrisLister.getSelectionModel().getSelectedItem();
		if (produkt == null || prisListe == null) {
			return;
		}

		ProduktWindow pw = new ProduktWindow("Opdatere Produkt", produkt, prisListe);
		pw.showAndWait();

		// Wait for the modal dialog to close

		int selectIndex = lvwProdukter.getSelectionModel().getSelectedIndex();
		lvwProdukter.getItems().setAll(Controller.getController().getProdukt());
		lvwProdukter.getSelectionModel().select(selectIndex);
		this.updateControls();
	}

	private void deleteProduktAction() {
		Produkt produkt = lvwProdukter.getSelectionModel().getSelectedItem();
		if (produkt != null) {
			Controller.getController().removeProdukt(produkt);

			lvwProdukter.getItems().setAll(Controller.getController().getProdukt());
			this.updateControls();
		}
	}

	// PrisListe buttons

	private void updatePrisListeAction() {
		PrisListe prisListe = lvwPrisLister.getSelectionModel().getSelectedItem();
		if (prisListe == null) {
			return;
		}

		PrisListeWindow plw = new PrisListeWindow("Opdatere PrisListe", prisListe);
		plw.showAndWait();

		// Wait for the modal dialog to close

		int selectIndex = lvwPrisLister.getSelectionModel().getSelectedIndex();
		lvwPrisLister.getItems().setAll(Controller.getController().getPrisListe());
		lvwPrisLister.getSelectionModel().select(selectIndex);
		this.updateControls();
	}

	private void deletePrisListeAction() {
		PrisListe prisListe = lvwPrisLister.getSelectionModel().getSelectedItem();
		if (prisListe != null) {
			// TODO
			// rette removeprisliste i controller (stavefejl)
			Controller.getController().deltePrisListe(prisListe);
			lvwPrisLister.getItems().setAll(Controller.getController().getPrisListe());
			lvwProdukter.getItems().clear();
		}
	}

	private void createPrisListeAction() {
		PrisListeWindow dia = new PrisListeWindow("Opret PrisListe");
		dia.showAndWait();

		// Wait for the modal dialog to close

		lvwPrisLister.getItems().setAll(Controller.getController().getPrisListe());
		int index = lvwPrisLister.getItems().size() - 1;
		lvwPrisLister.getSelectionModel().select(index);
		this.updateControls();
	}
}
