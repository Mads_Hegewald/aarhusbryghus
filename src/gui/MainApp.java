package gui;

import controller.Controller;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application {

	public static void main(String[] args) {

		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {

		Controller.getController().initContent();
		
		stage.setTitle("AarhusBryghus Kasse System");
		BorderPane pane = new BorderPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// ---------------------------------------------------------------------------------------------
	private Tab tabProdukter = new Tab("ProduktListe");
	private Tab tabTransaktioner = new Tab("SalgsOversigt");
	private Tab tabBestillinger = new Tab("BestillingsOversigt");

	private void initContent(BorderPane pane) {
		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		pane.setCenter(tabPane);
		
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		Tab tabBaren = new Tab("Barsalg");
		tabPane.getTabs().add(tabBaren);

		BarenPane barenPane = new BarenPane();
		tabBaren.setContent(barenPane);

		tabPane.getTabs().add(tabProdukter);
		ProduktPane produktPane = new ProduktPane();
		tabProdukter.setContent(produktPane);

		tabPane.getTabs().add(tabTransaktioner);
		TransaktionPane transaktionPane = new TransaktionPane();
		tabTransaktioner.setContent(transaktionPane);
		tabTransaktioner.setOnSelectionChanged(event -> transaktionPane.initSalg());

		tabPane.getTabs().add(tabBestillinger);
		BestillingPane bestillingPane = new BestillingPane();
		tabBestillinger.setContent(bestillingPane);
		tabBestillinger.setOnSelectionChanged(event -> bestillingPane.updateControls());

	}

}
