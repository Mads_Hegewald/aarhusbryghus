package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import Storage.Storage;
import model.Bestilling;
import model.Betaling;
import model.Betalingstype;
import model.Fadølsanlæg;
import model.Fustage;
import model.Klippekort;
import model.Kunde;
import model.PrisListe;
import model.PrisListeLinje;
import model.Produkt;
import model.Rundvisning;
import model.Transaktion;
import model.TransaktionsListeLinje;
import model.Øl;

public class Controller {

	private static Controller controller;
	private Storage storage = Storage.getStorage();

	public static Controller getController() {
		if (controller == null) {
			controller = new Controller();
		}
		return controller;
	}

	// -------------------------------Kunde-----------------------------------------
	public Kunde createKunde(String navn, String adresse, int tlfnr) {
		Kunde kunde = new Kunde(navn, adresse, tlfnr);
		storage.addKunde(kunde);
		return kunde;
	}

	public void updateKunde(Kunde kunde, String navn, String adresse, int tlfnr) {
		kunde.setNavn(navn);
		kunde.setAdresse(adresse);
		kunde.setTlfnr(tlfnr);
	}

	public void deleteKunde(Kunde kunde) {
		storage.removeKunde(kunde);
	}

	public List<Kunde> getKunder() {
		return storage.getKunder();
	}

	// -------------------------------Betaling-----------------------------------------
	public Betaling createBetaling(Double beløb, Betalingstype type) {
		Betaling betaling = new Betaling(beløb, type);
		return betaling;
	}

	public void updateKunde(Betaling betaling, Double beløb, Betalingstype type) {
		betaling.setBeløb(beløb);
		betaling.setType(type);
	}

	// -------------------------------PrisListe--------------------------------------
	public PrisListe createPrisListe(String beskrivelse) {
		PrisListe prisListe = new PrisListe(beskrivelse);
		storage.addPrisListe(prisListe);
		return prisListe;
	}

	public void updatePrisListe(PrisListe prisListe, String beskrivelse) {
		prisListe.setBeskrivelse(beskrivelse);
	}

	public void deltePrisListe(PrisListe prisListe) {
		storage.removePrisListe(prisListe);
	}

	public List<PrisListe> getPrisListe() {
		return storage.getPrisListe();
	}

	public void addPrisListeLinjeTilPrisListe(PrisListe prisListe, PrisListeLinje pll) {
		prisListe.addPrisListeLinje(pll);
	}

	public void removePrisListeLinjeFraPrisListe(PrisListe prisListe, PrisListeLinje pll) {
		prisListe.removePrisListeLinje(pll);
	}

	// -------------------------------Transaktion------------------------------------
	public Transaktion createTransaktion(LocalDate dato, int fakturaNr) {
		Transaktion transaktion = new Transaktion(dato, fakturaNr);
		storage.addTransaktion(transaktion);
		return transaktion;
	}

	public void updateTransaktion(Transaktion transaktion, LocalDate dato, int fakturaNr) {
		transaktion.setDato(dato);
		transaktion.setFakturaNr(fakturaNr);
	}

	public void deleteTransaktion(Transaktion transaktion) {
		storage.removeTransaktion(transaktion);
		if (transaktion.getKunde() != null) {
			transaktion.getKunde().removeTransaktion(transaktion);
		}
	}

	public List<Transaktion> getTransaktion() {
		return storage.getTransaktion();
	}

	public void addTransaktionTilKunde(Transaktion transaktion, Kunde kunde) {
		kunde.addTransaktioner(transaktion);
	}

	public void removeTransaktionFraKunde(Transaktion transaktion, Kunde kunde) {
		kunde.removeTransaktion(transaktion);
		transaktion.setKunde(null);
	}

	public void addTransaktionsListeLinjeTilTransaktion(Transaktion transaktion, TransaktionsListeLinje tll) {
		transaktion.addTll(tll);
	}

	// -------------------------------Produkt----------------------------------------
	public List<Produkt> getProdukt() {
		return storage.getProdukt();
	}

	public Produkt createProdukt(String beskrivelse) {
		Øl ol = new Øl(beskrivelse);
		Storage.getStorage().addProdukt(ol);
		return ol;
	}

	public Produkt createProdukt(String beskrivelse, Integer antalKlip) {
		Klippekort kk = new Klippekort(beskrivelse, antalKlip);
		Storage.getStorage().addProdukt(kk);
		return kk;
	}

	public Produkt createProdukt(String beskrivelse, double pant, int liter) {
		Fustage f = new Fustage(beskrivelse, pant, liter);
		Storage.getStorage().addProdukt(f);
		return f;
	}

	public void updateProdukt(Produkt produkt, String beskrivelse) {
		produkt.setBeskrivelse(beskrivelse);
	}

	public void updateProdukt(Produkt produkt, String beskrivelse, Integer antalKlip) {
		produkt.setBeskrivelse(beskrivelse);
		produkt.setAntalKlip(antalKlip);
	}

	public void updateProdukt(Produkt produkt, String beskrivelse, double pant, int liter) {
		produkt.setBeskrivelse(beskrivelse);
		produkt.setPant(pant);
		produkt.setLiter(liter);
	}

	public void removeProdukt(Produkt produkt) {
		produkt.setTllNull();
		for (int i = 0; i < getPrisListe().size(); i++) {
            for (int k = 0; k < getPrisListe().get(i).getPrisListeLinje().size(); k++) {
                if (getPrisListe().get(i).getPrisListeLinje().get(k).getProdukt() == produkt) {
                    getPrisListe().get(i).removePrisListeLinje(getPrisListe().get(i).getPrisListeLinje().get(k));
                }
            }
        }
		if (!produkt.getPrisListeLinje().isEmpty()) {
			for (int i = 0; i < produkt.getPrisListeLinje().size(); i++) {
				produkt.removePll(produkt.getPrisListeLinje().get(i));
								
			}
		}
		storage.removeProdukt(produkt);
	}

	public void addPrisListeLinjeTilProdukt(Produkt produkt, PrisListeLinje pll) {
		produkt.addPll(pll);
	}

	public void addTransaktionsListeLinjeTilProdukt(Produkt produkt, TransaktionsListeLinje tll) {
		produkt.setTll(tll);
	}

	// -------------------------------Bestilling-------------------------------------
	public Bestilling createRundvisningBestilling(String beskrivelse, String id, LocalDate startDato,
			LocalDate slutDato, int antalPersoner, LocalTime tidspunkt) {
		Bestilling rb = new Rundvisning(beskrivelse, id, startDato, slutDato, antalPersoner, tidspunkt);
		storage.addBestilling(rb);
		return rb;
	}

	// bestilling, afhentingsdato, aktuel afleveringsdato
	// der bliver pålagt 3 dages buffer på slutdato ved fadølsanlæg.

	public Bestilling createFadølsanlægBestilling(String id, LocalDate startDato, LocalDate slutDato) {
		for (int k = 0; k < storage.getFadølsanlæg().size(); k++) {
			storage.getFadølsanlæg().get(k).setAvailable(true);
		}

		for (int i = 0; i < storage.getBestilling().size(); i++) {
			if (!storage.getBestilling().get(i).getBasicBeskrivelse().contains("Fadølsanlæg")) {
			} else {
				LocalDate dato = storage.getBestilling().get(i).getSlutDato().plusDays(3);
				if (startDato.isAfter(dato)) {
					for (int j = 0; j < storage.getBestilling().get(i).getFadølsanlæg().size(); j++) {
						if (storage.getBestilling().get(i).getFadølsanlæg().get(j).isAvailable() == true) {

							storage.getBestilling().get(i).getFadølsanlæg().get(j).setAvailable(true);
						}
					}
				} else {
					for (int m = 0; m < storage.getBestilling().get(i).getFadølsanlæg().size(); m++) {

						storage.getBestilling().get(i).getFadølsanlæg().get(m).setAvailable(false);
					}
				}
			}
		}
		for (int l = 0; l < storage.getFadølsanlæg().size(); l++) {
			if (storage.getFadølsanlæg().get(l).isAvailable() == true) {
				Bestilling b = new Bestilling(storage.getFadølsanlæg().get(l).getBeskrivelse(), id, startDato,
						slutDato);
				b.addFadølsanlæg(storage.getFadølsanlæg().get(l));
				storage.getFadølsanlæg().get(l).setAvailable(false);
				storage.addBestilling(b);
				return b;
			}
		}

		return null;

	}

	public List<Bestilling> getBestillinger() {
		return storage.getBestilling();
	}

	public void removeBestilling(Bestilling bestilling) {
		bestilling.setTllNull();
		if (!bestilling.getPrisListeLinje().isEmpty()) {
			for (int i = 0; i < bestilling.getPrisListeLinje().size(); i++) {
				bestilling.removePll(bestilling.getPrisListeLinje().get(i));
			}
		}
		if (bestilling.getFadølsanlæg().size() > 0) {
			for (Fadølsanlæg f : bestilling.getFadølsanlæg()) {
				bestilling.removeFadølsanlæg(f);
				f.setAvailable(true);
			}

		}
		storage.removeBestilling(bestilling);
	}

	public void updateBestilling(Bestilling bestilling, String id, LocalDate startDato, LocalDate slutDato,
			int antalPersoner, LocalTime tidspunkt) {
		bestilling.setId(id);
		bestilling.setStartDato(startDato);
		bestilling.setSlutDato(slutDato);
		bestilling.setAntalPersoner(antalPersoner);
		bestilling.setTidspunkt(tidspunkt);

	}

	public void updateBestilling(Bestilling bestilling, String id, LocalDate startDato, LocalDate slutDato) {
		bestilling.setId(id);
		bestilling.setStartDato(startDato);
		bestilling.setSlutDato(slutDato);
	}

	// -------------------------------Fadølsanlæg-------------------------------------

	public Fadølsanlæg createFadølsanlæg(String beskrivelse, boolean available) {
		Fadølsanlæg f = new Fadølsanlæg(beskrivelse, available);
		storage.addFadølsanlæg(f);
		return f;
	}

	public void deleteFadølsanlæg(Fadølsanlæg fadølsanlæg) {
		storage.removeFadølsanlæg(fadølsanlæg);
	}

	public void updateFadølsanlæg(Fadølsanlæg fadølsanlæg, String beskrivelse, boolean available) {
		fadølsanlæg.setBeskrivelse(beskrivelse);
		fadølsanlæg.setAvailable(available);

	}

	public List<Fadølsanlæg> getFadølsanlæg() {
		return storage.getFadølsanlæg();
	}

	// -------------------------------TransaktionsListeLinje-------------------------------------
	public TransaktionsListeLinje createTransaktionsListeLinje(double pris, int antal) {
		TransaktionsListeLinje tll = new TransaktionsListeLinje(pris, antal);
		return tll;
	}

	public void addProduktTilTransaktionsListeLinje(TransaktionsListeLinje tll, Produkt produkt) {
		tll.setProdukt(produkt);
	}

	// -------------------------------PrisListeLinje-------------------------------------
	public PrisListeLinje createPrisListeLinje(double pris, String beskrivelse) {
		PrisListeLinje pll = new PrisListeLinje(pris, beskrivelse);
		return pll;
	}

	public void addProduktTilPrisListeLinje(PrisListeLinje pll, Produkt produkt) {
		pll.setProdukt(produkt);
	}

	// -------------------------------InitContent-------------------------------------

	public void initContent() {
		// oprettelse af objekter
		Bestilling rb = createRundvisningBestilling("Rundvisning", "2552", LocalDate.now(), LocalDate.now(), 50,
				LocalTime.now());
		Fadølsanlæg f1 = createFadølsanlæg("Fadølsanlæg 1", true);
		Fadølsanlæg f2 = createFadølsanlæg("Fadølsanlæg 2", false);
		Fadølsanlæg f6 = createFadølsanlæg("Fadølsanlæg 6", true);

		Bestilling b = createFadølsanlægBestilling("2553", LocalDate.now(), LocalDate.now());
		TransaktionsListeLinje tll = createTransaktionsListeLinje(200, 1);
		Transaktion t = createTransaktion(LocalDate.now(), 123321);
		TransaktionsListeLinje tll1 = createTransaktionsListeLinje(50, 1);

		Kunde k = createKunde("Hans", "Hansvej", 888);
		Kunde k1 = createKunde("Thomas", "Thomsvej", 123);
		Kunde k2 = createKunde("Allan", "Danasveh", 321);

		Produkt ol = createProdukt("øl");

		Produkt øl = createProdukt("Kloster");
		Produkt øl1 = createProdukt("Ale");
		Produkt øl2 = createProdukt("Blonde");
		Produkt f = createProdukt("Fustage", 200.0, 10);
//		Produkt g = createProdukt("Kloster");

		PrisListeLinje prisll = createPrisListeLinje(50.0, "Fredagsbar");

		PrisListeLinje prisll4 = createPrisListeLinje(75.0, "Fredagsbar");

		PrisListeLinje prisll5 = createPrisListeLinje(82.0, "Fredagsbar");

		PrisListeLinje prisll2 = createPrisListeLinje(100.0, "Butik");
		PrisListeLinje prisll3 = createPrisListeLinje(100.0, "Butik");

		PrisListe pl = createPrisListe("Fredagsbar");
		pl.addPrisListeLinje(prisll);
		pl.addPrisListeLinje(prisll4);
		pl.addPrisListeLinje(prisll5);
		PrisListe pl2 = createPrisListe("Butik");
		pl2.addPrisListeLinje(prisll2);
		pl2.addPrisListeLinje(prisll3);

		øl2.addPll(prisll5);
//		g.addPll(prisll4);
		øl.addPll(prisll);
		ol.addPll(prisll3);
		f.addPll(prisll2);
		øl1.addPll(prisll4);

		TransaktionsListeLinje tll2 = createTransaktionsListeLinje(øl.getPrisListeLinje().get(0).getPris(), 1);

		TransaktionsListeLinje tll3 = createTransaktionsListeLinje(øl1.getPrisListeLinje().get(0).getPris(), 1);

		TransaktionsListeLinje tll4 = createTransaktionsListeLinje(øl2.getPrisListeLinje().get(0).getPris(), 1);

		TransaktionsListeLinje tll5 = createTransaktionsListeLinje(f.getPrisListeLinje().get(0).getPris(), 1);

//		TransaktionsListeLinje tll7 = createTransaktionsListeLinje(g.getPrisListeLinje().get(0).getPris(), 1);

		TransaktionsListeLinje tll6 = createTransaktionsListeLinje(ol.getPrisListeLinje().get(0).getPris(), 10);

		Transaktion t4 = createTransaktion(LocalDate.of(2019, 04, 03), 3);

		Transaktion t1 = createTransaktion(LocalDate.of(2019, 04, 03), 2);

		Transaktion t3 = createTransaktion(LocalDate.now(), 4);

		Transaktion t5 = createTransaktion(LocalDate.now(), 4);

		Transaktion t6 = createTransaktion(LocalDate.now(), 4);
//				Betaling bet = opretBetaling (300.0, Betalingstype.KONTANT);

//				t3.addBetalinger(bet);
//				t3.addTll(tll2);
		t3.addTll(tll3);
		t6.addTll(tll6);
		t5.addTll(tll5);

		t4.addTll(tll4);

		t1.addTll(tll2);
//				t1.addTll(tll3);

		tll2.setProdukt(øl);
		tll3.setProdukt(øl1);
		tll4.setProdukt(øl2);
		tll5.setProdukt(f);
		tll6.setProdukt(ol);
//		tll7.setProdukt(g);

		double pris = t1.getSamletBeløb();
		t1.setSamletBeløb(pris);

		// link af objekter
		addTransaktionTilKunde(t, k);
		addTransaktionsListeLinjeTilTransaktion(t, tll);
		addTransaktionsListeLinjeTilProdukt(b, tll);
		addTransaktionsListeLinjeTilProdukt(rb, tll1);
		addTransaktionsListeLinjeTilTransaktion(t, tll1);

	}
}
