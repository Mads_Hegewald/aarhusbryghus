package model;

import java.util.ArrayList;

public class PrisListe {
	private String beskrivelse;
	private ArrayList<PrisListeLinje> pll = new ArrayList<>();

	public PrisListe(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public ArrayList<PrisListeLinje> getPrisListeLinje() {
		return new ArrayList<>(pll);
	}

	public void addPrisListeLinje(PrisListeLinje p) {
		if (!pll.contains(p)) {
			pll.add(p);
		}
	}

	public void removePrisListeLinje(PrisListeLinje p) {
		if (pll.contains(p)) {
			pll.remove(p);
		}
	}
	
	@Override
	public String toString() {
		return beskrivelse;
	}
}
