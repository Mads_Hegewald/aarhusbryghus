package model;

public class Fustage extends Produkt {
	private Double pant;
	private int liter;

	public Fustage(String beskrivelse, double pant, int liter) {
		super(beskrivelse);
		this.pant = pant;
		this.liter = liter;
	}

	@Override
	public Double getPant() {
		return pant;
	}

	@Override
	public void setPant(Double pant) {
		this.pant = pant;
	}

	@Override
	public int getLiter() {
		return liter;
	}

	@Override
	public void setLiter(int liter) {
		this.liter = liter;
	}

	@Override
	public String toString() {
		return beskrivelse;
	}

}
