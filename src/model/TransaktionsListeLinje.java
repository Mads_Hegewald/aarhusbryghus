package model;

public class TransaktionsListeLinje {
	private Double pris;
	private int antal;
	private Produkt produkt;

	public TransaktionsListeLinje(Double pris, int antal) {
		super();
		this.pris = pris;
		this.antal = antal;
	}

	public Double getPris() {
		return pris;
	}

	public void setPris(Double pris) {
		this.pris = pris;
	}

	public int getAntal() {
		return antal;
	}

	public void setAntal(int antal) {
		this.antal = antal;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		if (this.produkt != produkt) {
			Pre.require(this.produkt == null);
			this.produkt = produkt;
			produkt.setTll(this);

		}
	}

}
