package model;

import java.util.ArrayList;

public abstract class Produkt {
	protected String beskrivelse;
	private ArrayList<PrisListeLinje> pll = new ArrayList<>();
	private TransaktionsListeLinje tll;

	public Produkt(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public ArrayList<PrisListeLinje> getPrisListeLinje() {
		return new ArrayList<>(pll);
	}

	public void addPll(PrisListeLinje p) {
		if (!pll.contains(p)) {
			pll.add(p);
			p.setProdukt(this);
		}
	}

	public void removePll(PrisListeLinje p) {
		if (pll.contains(p)) {
			pll.remove(p);
			p.setProdukt(null);
		}
	}

	public TransaktionsListeLinje getTll() {
		return tll;
	}

	public void setTll(TransaktionsListeLinje tll) {
		if (this.tll != tll) {
			Pre.require(this.tll == null);
			this.tll = tll;
			tll.setProdukt(this);

		}
	}

	public void setTllNull() {
		if (this.tll != null) {
			TransaktionsListeLinje oldTll = this.tll;
			this.tll = null;
			oldTll.setProdukt(this);
		}
	}

	@Override
	public abstract String toString();

	public int getAntalKlip() {
		return -1;
	}

	public Double getPant() {
		return -1.0;
	}

	public int getLiter() {
		return -1;
	}

	public void setAntalKlip(int antalKlip) {
	}

	public void setLiter(int liter) {
	}

	public void setPant(Double pant) {
	}

	private static void swap(ArrayList<Produkt> list, int i, int j) {
		Produkt temp = list.get(i);
		list.set(i, list.get(j));
		list.set(j, temp);
	}

	public static void selectionSort(ArrayList<Produkt> list) {
		for (int i = 0; i < list.size(); i++) {
			int minPos = i;
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(j).getBeskrivelse().compareTo(list.get(minPos).getBeskrivelse()) < 0) {
					minPos = j;
				}

			}
			swap(list, i, minPos);
		}
	}

}
