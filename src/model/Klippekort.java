package model;

public class Klippekort extends Produkt {
	private int antalKlip;

	public Klippekort(String beskrivelse, int antalKlip) {
		super(beskrivelse);
		this.antalKlip = antalKlip;
	}

	@Override
	public int getAntalKlip() {
		return antalKlip;
	}
	@Override
	public void setAntalKlip(int antalKlip) {
		this.antalKlip = antalKlip;
	}

	@Override
	public String toString() {
		return beskrivelse + " " + antalKlip;
	}

}
