package model;

import java.util.ArrayList;

public class Fadølsanlæg {
	private boolean available;
	private String beskrivelse;
	private ArrayList<Bestilling> bestillinger = new ArrayList<>();

	public Fadølsanlæg(String beskrivelse, boolean available) {
		this.beskrivelse = beskrivelse;
		this.available = available;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public boolean isAvailable() {
		return available;
	}

	public void addBestilling(Bestilling bestilling) {
		if (!bestillinger.contains(bestilling)) {
			bestillinger.add(bestilling);
			bestilling.addFadølsanlæg(this);
		}
	}

	public void removeBestilling(Bestilling bestilling) {
		if (bestillinger.contains(bestilling)) {
			bestillinger.remove(bestilling);
			bestilling.removeFadølsanlæg(this);
		}
	}

	public ArrayList<Bestilling> getBestillinger() {
		return new ArrayList<>(bestillinger);
	}

}
