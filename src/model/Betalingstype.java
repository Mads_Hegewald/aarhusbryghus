package model;

public enum Betalingstype {
	DANKORT, KONTANT, MOBILEPAY, KLIPPEKORT;
}
