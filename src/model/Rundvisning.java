package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Rundvisning extends Bestilling {
	private int antalPersoner;
	private LocalTime tidspunkt;

	public Rundvisning(String beskrivelse, String id, LocalDate startDato, LocalDate slutDato, int antalPersoner,
			LocalTime tidspunkt) {
		super(beskrivelse, id, startDato, slutDato);
		this.antalPersoner = antalPersoner;
		this.tidspunkt = tidspunkt;
	}

	@Override
	public int getAntalPersoner() {
		return antalPersoner;
	}

	@Override
	public void setAntalPersoner(int antalPersoner) {
		this.antalPersoner = antalPersoner;
	}

	@Override
	public LocalTime getTidspunkt() {
		return tidspunkt;
	}

	@Override
	public void setTidspunkt(LocalTime tidspunkt) {
		this.tidspunkt = tidspunkt;
	}

	@Override
	public String getBeskrivelse() {
		String s = format.format(startDato);
		String s1 = format.format(slutDato);
		String s2 = tidspunkt.format(DateTimeFormatter.ofPattern(" HH : mm : ss"));
		return "" + beskrivelse + " " + antalPersoner + " " + s + " " + s1 + " " + s2;
	}

}
