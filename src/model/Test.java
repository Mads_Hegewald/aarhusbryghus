package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Iterator;

import Storage.Storage;
import controller.Controller;

public class Test {

	public static void main(String[] args) {

		Controller controller = Controller.getController();
		Storage storage = Storage.getStorage();
		
		PrisListe pl4 = controller.createPrisListe("Hat og briller");
		PrisListe pl5 = controller.createPrisListe("Bibliotek");
		
		for(PrisListe p : Storage.getStorage().getPrisListe()) {
		System.out.println(p.getBeskrivelse());
		}
		
		// Test af PrisListe, PrisListeLinje, Produkt (w/o Bestilling), Transaktion og
		// TransaktionsListeLinje

		PrisListe pl = new PrisListe("Fredagsbar");

		

		

		Øl øl = new Øl("Kloster");
		
		PrisListeLinje prisll = new PrisListeLinje(50.0, "Fredagsbar");
		
		pl.addPrisListeLinje(prisll);

		Fustage f = new Fustage("Fustage", 200, 10);

		PrisListeLinje prisll2 = new PrisListeLinje(100.0, "Butik");

		pl.addPrisListeLinje(prisll2);

		øl.addPll(prisll);
	
		f.addPll(prisll2);

		System.out.println(øl.getPrisListeLinje().get(0).getBeskrivelse() + " " + øl.getPrisListeLinje().get(0).getPris());

		TransaktionsListeLinje tll = new TransaktionsListeLinje(øl.getPrisListeLinje().get(0).getPris(), 2);

		TransaktionsListeLinje tll1 = new TransaktionsListeLinje(f.getPrisListeLinje().get(0).getPris(), 2);

		Transaktion t = new Transaktion(LocalDate.now(), 2);
		
		Transaktion t3 = new Transaktion(LocalDate.now(), 4);
		Betaling bet = new Betaling (300.0, Betalingstype.KONTANT);
		
		t3.addBetalinger(bet);
		t3.addTll(tll);
		t3.addTll(tll1);
	
		
		t.addTll(tll);
		t.addTll(tll1);

		tll.setProdukt(øl);
		tll1.setProdukt(f);

		double pris = t.getSamletBeløb();
		t.setSamletBeløb(pris);
		

//		t3.udskrivKvittering();
//		System.out.println(t.getSamletBeløb());

//		t.udskrivKvittering();

//		for (int i = 0; i < pl.getPrisListeLinje().size(); i++) {
//			if (pl.getBeskrivelse().equals(pl.getPrisListeLinje().get(i).getBeskrivelse())) {
//				System.out.println(pl.getPrisListeLinje().get(i).getProdukt().getBeskrivelse() + " "
//						+ pl.getPrisListeLinje().get(i).getPris());
//			}
//		}

		// Test af Bestilling og kunde
		Kunde k = new Kunde("Hans", "Hansvej", 123);

		Bestilling r = controller.createRundvisningBestilling("Rundvisning", "5", LocalDate.of(2019, 10, 10), LocalDate.of(2019, 10, 15), 72, LocalTime.now());
		
		
		

		Fadølsanlæg fAnlæg = new Fadølsanlæg("2", true);

		Transaktion t1 = new Transaktion(LocalDate.now(), 3);

		TransaktionsListeLinje tll3 = new TransaktionsListeLinje(200.0, 1);

		TransaktionsListeLinje tll4 = new TransaktionsListeLinje(300.0, 1);

		// rundvisning
		r.setTll(tll3);

		t1.addTll(tll3);

		// transaktion på kunde
		t1.setKunde(k);

		// fafølsanlæg
//		fAnlæg.setTll(tll4);

		t1.addTll(tll4);

//		System.out.println(k.getTransaktioner().get(0).getFakturaNr());
//		System.out
//				.println(k.getTransaktioner().get(0).getTransaktionsListeLinje().get(0).getProdukt().getBeskrivelse());
//		System.out
//				.println(k.getTransaktioner().get(0).getTransaktionsListeLinje().get(1).getProdukt().getBeskrivelse());


		// test af erhvervskunde getCvrNr
//		System.out.println(eK.getCvrNr());
		
		controller.createFadølsanlæg("Anlæg 2", false);
		controller.createFadølsanlæg("Anlæg 6", true);
		
		controller.createFadølsanlægBestilling("5", LocalDate.of(2019, 10, 10), LocalDate.of(2019, 10, 15)); 
		controller.createFadølsanlægBestilling("6", LocalDate.of(2019, 10, 13), LocalDate.of(2019, 10, 20)); 
		controller.createFadølsanlægBestilling("7", LocalDate.of(2019, 10, 21), LocalDate.of(2019, 10, 24)); 
		controller.createFadølsanlægBestilling("8", LocalDate.of(2019, 10, 22), LocalDate.of(2019, 10, 27)); 
		controller.createFadølsanlægBestilling("9", LocalDate.of(2019, 10, 27), LocalDate.of(2019, 10, 27)); 
		
		
		//Act
//		for(int i = 0; i < storage.getBestilling().size(); i++) {
//			if(!storage.getBestilling().get(i).getBeskrivelse().equals(null)) {
//				System.out.println(storage.getBestilling().get(i).getBeskrivelse());
//			}
//		}
		
		String s = storage.getBestilling().get(0).getBasicBeskrivelse();
		String s1 = storage.getBestilling().get(1).getBasicBeskrivelse();
		String s2 = storage.getBestilling().get(2).getBasicBeskrivelse();
		String s3 = storage.getBestilling().get(3).getBasicBeskrivelse();
		String s4 = storage.getBestilling().get(3).getBasicBeskrivelse();
		
		System.out.println(storage.getBestilling().size());

		// Creating an empty HashSet 
        HashSet<String> set = new HashSet<String>(); 
  
        // Use add() method to add elements into the Set 
        set.add("Welcome"); 
        set.add("To"); 
        set.add("Geeks"); 
        set.add("4"); 
        set.add("Geeks"); 
  
        // Displaying the HashSet 
        System.out.println("HashSet: " + set); 
  
        // Creating an iterator 
        Iterator value = set.iterator(); 
  
        // Displaying the values after iterating through the set 
        System.out.println("The iterator values are: "); 
        while (value.hasNext()) { 
            System.out.println(value.next()); 
        } 
    
		
	}

}
