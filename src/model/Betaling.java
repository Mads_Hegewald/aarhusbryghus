package model;

public class Betaling {

	private Double beløb;
	private Betalingstype type;

	public Betaling(Double beløb, Betalingstype type) {

		this.beløb = beløb;
		this.type = type;
	}

	public Betalingstype getType() {
		return type;
	}

	public void setType(Betalingstype type) {
		this.type = type;
	}

	public Double getBeløb() {
		return beløb;
	}

	public void setBeløb(Double beløb) {
		this.beløb = beløb;
	}

}
