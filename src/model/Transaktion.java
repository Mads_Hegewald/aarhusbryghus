package model;

import java.time.LocalDate;
import java.util.ArrayList;

public class Transaktion {
	private LocalDate dato;
	private Double samletBeløb;
	private int fakturaNr;
	private Kunde kunde;
	private ArrayList<Betaling> betalinger = new ArrayList<>();
	private ArrayList<TransaktionsListeLinje> tll = new ArrayList<>();

	public Transaktion(LocalDate dato, int fakturaNr) {
		this.dato = dato;
		this.fakturaNr = fakturaNr;
	}

	public LocalDate getDato() {
		return dato;
	}

	public void setDato(LocalDate dato) {
		this.dato = dato;
	}

	public Double getSamletBeløb() {
		samletBeløb = 0.0;
		for (TransaktionsListeLinje t : tll) {
			samletBeløb += (t.getPris() * t.getAntal());
		}
		return samletBeløb;
	}

	public void setSamletBeløb(Double samletBeløb) {
		this.samletBeløb = samletBeløb;
	}

	public int getFakturaNr() {
		return fakturaNr;
	}

	public void setFakturaNr(int fakturaNr) {
		this.fakturaNr = fakturaNr;
	}

	public Kunde getKunde() {
		return kunde;
	}

	public void setKunde(Kunde kunde) {
		this.kunde = kunde;
		kunde.addTransaktioner(this);
	}

	public ArrayList<Betaling> getBetalinger() {
		return new ArrayList<>(betalinger);
	}

	public void addBetalinger(Betaling betaling) {
		if (!betalinger.contains(betaling)) {
			betalinger.add(betaling);
		}
	}

	public ArrayList<TransaktionsListeLinje> getTransaktionsListeLinje() {
		return new ArrayList<>(tll);
	}

	public void addTll(TransaktionsListeLinje t) {
		if (!tll.contains(t)) {
			tll.add(t);
		}
	}

	public void udskrivKvittering() {
		System.out.println("Faktura Nummer " + fakturaNr);
		System.out.println("Dato: " + dato);
		for (TransaktionsListeLinje t : tll) {
			System.out
					.println(t.getAntal() + " " + t.getProdukt().getBeskrivelse() + " " + (t.getAntal() * t.getPris()));
		}
		System.out.println("Total beløb: " + getSamletBeløb());
		if (kunde != null)
			System.out.println("Kunde: " + kunde.getNavn());
		if (betalinger.size() > 0) {
			for (Betaling b : betalinger) {
				System.out.println("Betaling: " + b.getType() + " " + b.getBeløb() + " " + "Kr.");
			}
		}
	}

}
