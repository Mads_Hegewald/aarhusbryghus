package model;

public class PrisListeLinje {
	private String beskrivelse;
	private Double pris;
	private Produkt produkt;

	public PrisListeLinje(Double pris, String beskrivelse) {
		this.pris = pris;
		this.beskrivelse = beskrivelse;
		
	}

	public Double getPris() {
		return pris;
	}

	public void setPris(Double pris) {
		this.pris = pris;
	}

	public String getBeskrivelse() {
		return beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}

}
