package model;

import java.util.ArrayList;

public class Kunde {
	private String navn;
	private String adresse;
	private int tlfnr;
	private ArrayList<Transaktion> transaktioner = new ArrayList<>();
	private int cvr;

	public Kunde(String navn, String adresse, int tlfnr) {
		this.navn = navn;
		this.adresse = adresse;
		this.tlfnr = tlfnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public int getTlfnr() {
		return tlfnr;
	}

	public void setTlfnr(int tlfnr) {
		this.tlfnr = tlfnr;
	}

	public ArrayList<Transaktion> getTransaktioner() {
		return new ArrayList<>(transaktioner);
	}

	public void addTransaktioner(Transaktion transaktion) {
		if (!transaktioner.contains(transaktion)) {
			transaktioner.add(transaktion);
			transaktion.setKunde(this);
		}
	}

	public void removeTransaktion(Transaktion transaktion) {
		if (transaktioner.contains(transaktion)) {
			transaktioner.remove(transaktion);
			transaktion.setKunde(null);
		}
	}

	public int getCvrNr() {
		return cvr;
	}

	@Override
	public String toString() {
		return navn;
	}

	private static void swap(ArrayList<Kunde> list, int i, int j) {
		Kunde temp = list.get(i);
		list.set(i, list.get(j));
		list.set(j, temp);
	}

	public static void selectionSort(ArrayList<Kunde> list) {
		for (int i = 0; i < list.size(); i++) {
			int minPos = i;
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(j).getNavn().compareTo(list.get(minPos).getNavn()) < 0) {
					minPos = j;
				}

			}
			swap(list, i, minPos);
		}
	}

	public void setCvrNr(int cvrNr) {
		this.cvr = cvrNr;
	}

}
