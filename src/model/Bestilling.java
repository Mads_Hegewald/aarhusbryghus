package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Bestilling extends Produkt {

	protected LocalDate startDato;
	protected LocalDate slutDato;
	protected String id;
	protected DateTimeFormatter format = DateTimeFormatter.ofPattern("dd MMMM yyyy");
	private ArrayList<Fadølsanlæg> fadølsanlæg = new ArrayList<>();

	public Bestilling(String beskrivelse, String id, LocalDate startDato, LocalDate slutDato) {
		super(beskrivelse);
		this.slutDato = slutDato;
		this.startDato = startDato;
		this.id = id;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getStartDato() {
		return startDato;
	}

	public void setStartDato(LocalDate startDato) {
		this.startDato = startDato;
	}

	public LocalDate getSlutDato() {
		return slutDato;
	}

	public void setSlutDato(LocalDate slutDato) {
		this.slutDato = slutDato;
	}

	@Override
	public String getBeskrivelse() {
		String s = format.format(startDato);
		String s1 = format.format(slutDato);
		return "" + beskrivelse + " " + s + " " + s1;
	}

	public String getBasicBeskrivelse() {
		return beskrivelse;
	}

	public void addFadølsanlæg(Fadølsanlæg f) {
		if (!fadølsanlæg.contains(f)) {
			fadølsanlæg.add(f);
			f.addBestilling(this);
		}
	}

	public void removeFadølsanlæg(Fadølsanlæg f) {
		if (fadølsanlæg.contains(f)) {
			fadølsanlæg.remove(f);
			f.removeBestilling(this);
		}
	}

	public ArrayList<Fadølsanlæg> getFadølsanlæg() {
		return new ArrayList<>(fadølsanlæg);
	}

	@Override
	public String toString() {
		return beskrivelse + " " + id;
	}

	public int getAntalPersoner() {
		return -1;
	}

	public void setAntalPersoner(int antalPersoner) {

	}

	public void setTidspunkt(LocalTime tidspunkt) {
	}

	public LocalTime getTidspunkt() {
		return null;
	}

}
